# Vitya PHP application framework base components

This package contains a collection of building blocks for a service
container-based web application or framework. They are designed to work within
the [Vitya application framework](https://gitlab.com/vitya-system/application).

The easiest way to start with the framework is to install
[the basic application template](https://gitlab.com/vitya-system/application-template).

Need a full-fledged CMS? Then have a look at
[the CMS application template](https://gitlab.com/vitya-system/cms-template).

Vitya aims to provide a simple CMS that can be easily extended. We try to keep
the code small and easy to understand.
