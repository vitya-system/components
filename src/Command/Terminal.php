<?php

/*
 * Copyright 2020 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\Component\Command;

class Terminal
{
    private $bgColors = [];
    private $fgColors = [];
    private $stdin = null;
    private $stdout = null;
    private $stderr = null;

    public function __construct()
    {
        $this->bgColors = [
            'success' => '42',
            'info' => '44',
            'error' => '41',
        ];
        $this->fgColors = [
            'success' => '32;1',
            'info' => '34;1',
            'error' => '31;1',
        ];
        $this->stdin = fopen('php://stdin', 'r');
        $this->stdout = fopen('php://stdout', 'w');
        $this->stderr = fopen('php://stderr', 'w');
    }

    public function text(string $s, $output = null): void
    {
        $output = ($output === null ? $this->stdout : $output);
        fwrite($output, $s);
    }

    public function success(string $s, $output = null): void
    {
        $output = ($output === null ? $this->stdout : $output);
        fwrite($output, $this->getColoredText($s, null, 'success'));
    }

    public function info(string $s, $output = null): void
    {
        $output = ($output === null ? $this->stdout : $output);
        fwrite($output, $this->getColoredText($s, null, 'info'));
    }

    public function error(string $s, $output = null): void
    {
        $output = ($output === null ? $this->stdout : $output);
        fwrite($output, $this->getColoredText($s, null, 'error'));
    }

    public function nl($output = null): void
    {
        $output = ($output === null ? $this->stdout : $output);
        fwrite($output, "\r\n");
    }

    public function getColoredText(string $s, string $bg = null, string $fg = null): string
    {
        $colored_s = '';
        if ($bg !== null && isset($this->bgColors[$bg])) {
            $colored_s .= "\033[" . $this->bgColors[$bg] . 'm';
        }
        if ($fg !== null && isset($this->fgColors[$fg])) {
            $colored_s .= "\033[" . $this->fgColors[$fg] . 'm';
        }
        $colored_s .= $s;
        $colored_s .= "\033[0m";
        return $colored_s;
    }

}
