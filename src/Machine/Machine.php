<?php

/*
 * Copyright 2020, 2024 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\Component\Machine;

use Exception;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\StreamFactoryInterface;
use Vitya\Component\Service\DependencyInjectorInterface;
use Vitya\Component\Route\ProcessingInstruction;

class Machine implements MachineInterface
{
    private $applicationParameters = [];
    private $dependencyInjector = null;
    private $responseFactory = null;
    private $streamFactory = null;

    public function __construct(
        DependencyInjectorInterface $dependency_injector,
        ResponseFactoryInterface $response_factory,
        StreamFactoryInterface $stream_factory,
        array $application_parameters
    ) {
        $this->applicationParameters = $application_parameters;
        $this->dependencyInjector = $dependency_injector;
        $this->responseFactory = $response_factory;
        $this->streamFactory = $stream_factory;
    }

    public function getDependencyInjector(): DependencyInjectorInterface
    {
        return $this->dependencyInjector;
    }

    public function getResponseFactory(): ResponseFactoryInterface
    {
        return $this->responseFactory;
    }

    public function getStreamFactory(): StreamFactoryInterface
    {
        return $this->streamFactory;
    }

    public function handle(RequestInterface $request): ResponseInterface
    {
        $processing_instruction = null;
        // If we are dealing with a server request, a processing instruction
        // may be attached to it.
        // Most probable case: we are processing the main request.
        if ($request instanceof ServerRequestInterface) {
            $processing_instruction = $request->getAttribute('Processing-Instruction');
            if (!$processing_instruction instanceof ProcessingInstruction) {
                throw new Exception(
                    'The Processing-Instruction attribute attached to this server request should be a ProcessingInstruction object.'
                );
            }
        }
        // If no processing instruction was found, try to create one from the
        // url.
        // Most probable case: we are processing a subrequest.
        if ($processing_instruction === null) {
            $uri = $request->getUri();
            if ($uri !== null && $uri->getPath() == '/_controller') {
                parse_str($uri->getQuery(), $query_params);
                if (isset($query_params['callable'])) {
                    if (!isset($query_params['parameters'])) {
                        $query_params['parameters'] = [];
                    }
                    $processing_instruction = new ProcessingInstruction(
                        (string) $query_params['callable'],
                        (array) $query_params['parameters'],
                    );
                } else {
                    throw new Exception(
                        'Invalid query string for the /_controller url.'
                    );
                }
            }
        }
        // At this point, we should know which controller to call.
        if ($processing_instruction === null) {
            throw new Exception;('No processing instruction could be determined for the request.');
        }
        while (null !== $processing_instruction) {
            $callable = $processing_instruction->getCallable();
            if ($callable === '') {
                throw new Exception;('No callable attached to the request.');
            }
            $callable_elements = explode('::', $callable);
            if (count($callable_elements) !== 2) {
                throw new Exception(
                    'Invalid callable (' . $callable . '). Callables must be expressed as a class::method string.'
                );
            }
            $named_parameters = $processing_instruction->getParameters() + [
                'app_params' => $this->applicationParameters,
                'request' => $request
            ];
            $callable_class = $callable_elements[0];
            $callable_method = $callable_elements[1];
            if (method_exists($callable_class, '__construct')) {
                $constructor_arguments = $this->dependencyInjector->getArguments($callable_class . '::__construct', $named_parameters);
                $controller_object = new $callable_class(...array_values($constructor_arguments));
            } else {
                $controller_object = new $callable_class();
            }
            $arguments = $this->dependencyInjector->getArguments($callable, $named_parameters);
            $response = call_user_func_array([$controller_object, $callable_method], $arguments);
            if ($response instanceof ProcessingInstruction) {
                $processing_instruction = $response;
            } else {
                $processing_instruction = null;
            }
        }
        if ($response instanceof ResponseInterface) {
            return $response;
        } elseif (null === $response || is_scalar($response)) {
            $body = $this->streamFactory->createStream((string) $response);
            $response_object = $this->responseFactory->createResponse()
                ->withHeader('Content-Type', 'text/plain;charset=UTF-8')
                ->withBody($body)
            ;
            return $response_object;
        }
        $var_type = gettype($response);
        if (is_object($response)) {
            $var_type = get_class($response) . ' object';
        }
        throw new Exception(''
            . 'The controller (' . $callable . ') did not return a valid response '
            . '(ResponseInterface object, scalar or null expected, ' . $var_type . ' returned instead).'
        );
    }

}
