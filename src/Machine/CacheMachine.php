<?php

/*
 * Copyright 2020 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\Component\Machine;

use DateTime;
use DateTimeInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\StreamFactoryInterface;
use Psr\SimpleCache\CacheInterface;

class CacheMachine implements MachineInterface
{
    private $cache = null;
    private $responseFactory = null;
    private $streamFactory = null;
    private $upstreamMachine = null;
    private $clientAllowedToBypassCache = true;

    public function __construct(
        MachineInterface $upstream_machine,
        CacheInterface $cache,
        ResponseFactoryInterface $response_factory,
        StreamFactoryInterface $stream_factory,
        bool $client_allowed_to_bypass_cache
    ) {
        $this->cache = $cache;
        $this->responseFactory = $response_factory;
        $this->streamFactory = $stream_factory;
        $this->upstreamMachine = $upstream_machine;
        $this->clientAllowedToBypassCache = $client_allowed_to_bypass_cache;
    }

    public function clientIsAllowedToBypassCache(): bool
    {
        return $this->clientAllowedToBypassCache;
    }

    public function handle(RequestInterface $request): ResponseInterface
    {
        if (false === $this->cacheCanBeUsed($request)) {
            return $this->getFreshResponse($request);
        }
        $cached_response = $this->getCachedResponse($request);
        if ($cached_response === null) {
            $final_response = $this->getFreshResponse($request);
            if ($this->responseCanBeCached($request, $final_response)) {
                $this->cacheResponse($request, $final_response);
            }
        } else {
            if ($this->responseNeedsValidation($request, $cached_response)) {
                $final_response = $this->getValidatedResponse($request, $cached_response);
                if ($this->responseCanBeCached($request, $final_response)) {
                    $this->cacheResponse($request, $final_response);
                }
            } else {
                $final_response = $this->getUpdatedCachedResponse($cached_response);
            }
        }
        return $final_response;
    }

    public function getUpstreamMachine(): MachineInterface
    {
        return $this->upstreamMachine;
    }

    public function getFreshResponse($request): ResponseInterface
    {
        return $fresh_response = $this->upstreamMachine
            ->handle($request)
            ->withHeader('Date', date(DATE_RFC7231))
            ->withHeader('Age', '0');
    }

    public function getCacheItemKey(RequestInterface $request): string
    {
        $cache_key_elements = [
            'cache_machine',
            $request->getMethod(),
            (string) $request->getUri(),
        ];
        return serialize($cache_key_elements);
    }

    public function cacheCanBeUsed(RequestInterface $request): bool
    {
        if (!in_array($request->getMethod(), ['GET', 'HEAD'])) {
            return false;
        }
        if (false === $this->clientAllowedToBypassCache) {
            return true;
        }
        $request_cache_control = $request->getHeader('Cache-Control');
        if (!empty($request_cache_control)) {
            $request_directives = explode(',', implode(', ', $request_cache_control));
            $request_directives = array_map('trim', $request_directives);
            $request_directives = array_map('mb_strtolower', $request_directives);
            if (in_array('no-store', $request_directives)) {
                return false;
            }
        }
        $request_authorization = $request->getHeader('Authorization');
        if (!empty($request_authorization)) {
            return false;
        }
        return true;
    }

    public function getCachedResponse(RequestInterface $request): ?ResponseInterface
    {
        $item = $this->cache->get($this->getCacheItemKey($request));
        if (!is_array($item)) {
            return null;
        }
        if (!isset($item['status_code']) || !is_int($item['status_code'])) {
            return null;
        }
        if (!isset($item['reason_phrase']) || !is_string($item['reason_phrase'])) {
            return null;
        }
        if (!isset($item['headers']) || !is_array($item['headers'])) {
            return null;
        }
        if (!isset($item['body']) || !is_string($item['body'])) {
            return null;
        }
        $response = $this->responseFactory
            ->createResponse($item['status_code'], $item['reason_phrase']);
        foreach ($item['headers'] as $k => $v) {
            $response = $response->withHeader($k, $v);
        }
        $response = $response->withBody($this->streamFactory->createStream($item['body']));
        return $response;
    }

    public function getUpdatedCachedResponse(ResponseInterface $response): ResponseInterface
    {
        if ($response->hasHeader('Date')) {
            $date_header = $response->getHeader('Date');
            if (isset($date_header[0])) {
                $dt = DateTime::createFromFormat(DateTimeInterface::RFC7231, $date_header[0]);
                if ($dt !== false) {
                    $ts = (int) $dt->format('U');
                    $now = time();
                    $age = $now - $ts;
                    if ($age < 0) {
                        $age = 0;
                    }
                    $response = $response
                        ->withHeader('Date', date(DATE_RFC7231, $now))
                        ->withHeader('Age', (string) $age);
                }
            }
        }
        return $response;
    }

    public function responseNeedsValidation(RequestInterface $request, ResponseInterface $response): bool
    {
        $response = $this->getUpdatedCachedResponse($response);
        $request_cache_control = $request->getHeader('Cache-Control');
        $request_directives = explode(',', implode(', ', $request_cache_control));
        $request_directives = array_map('trim', $request_directives);
        $request_directives = array_map('mb_strtolower', $request_directives);
        $response_cache_control = $response->getHeader('Cache-Control');
        $response_directives = explode(',', implode(', ', $response_cache_control));
        $response_directives = array_map('trim', $response_directives);
        $response_directives = array_map('mb_strtolower', $response_directives);
        // First, we need to determine the age of the response. If we can't,
        // we will need a validation.
        $response_age = -1;
        if ($response->hasHeader('Age')) {
            $response_age_header = $response->getHeader('Age');
            $response_age = (int) $response_age_header[0];
        }
        if ($response_age < 0) {
            return true;
        }
        // Then we try to determine whether the response is past its expiration
        // date. If we can"t find any cache control directive that we can use,
        // then we'll always ask for a validation.
        $no_usable_directive_found = true;
        $is_stale = false;
        $needs_validation = false;
        $response_max_age = 0;
        $response_s_maxage = 0;
        foreach ($response_directives as $response_directive) {
            if ($response_directive === 'no-cache') {
                $needs_validation = true;
            }
            if (preg_match('/s-maxage=([0-9]+)/', $response_directive, $matches)) {
                $response_s_maxage = (int) $matches[1];
                $no_usable_directive_found = false;
                if ($response_age > $response_s_maxage) {
                    $is_stale = true;
                }
            }
            if (preg_match('/max-age=([0-9]+)/', $response_directive, $matches)) {
                $response_max_age = (int) $matches[1];
                $no_usable_directive_found = false;
                if ($response_age > $response_max_age) {
                    $is_stale = true;
                }
            }
        }
        if ($is_stale) {
            $needs_validation = true;
        }
        $response_global_maxage = min($response_max_age, $response_s_maxage);
        foreach ($request_directives as $request_directive) {
            if (preg_match('/max-age=([0-9]+)/', $request_directive, $matches)) {
                $max_age = (int) $matches[1];
                $no_usable_directive_found = false;
                if ($response_age > $max_age) {
                    $needs_validation = true;
                }
            }
            if (preg_match('/min-fresh=([0-9]+)/', $request_directive, $matches)) {
                $min_fresh = (int) $matches[1];
                $no_usable_directive_found = false;
                if (($response_age + $min_fresh) > $response_global_maxage) {
                    $needs_validation = true;
                }
            }
        }
        if ($no_usable_directive_found) {
            return true;
        }
        // Now, we know whether the response is fresh or stale. Some directives
        // can still influence what we need to do.
        foreach ($request_directives as $request_directive) {
            if ($is_stale) {
                if (preg_match('/max-stale=?([0-9]*)/', $request_directive, $matches)) {
                    if ($matches[1] === '') {
                        $needs_validation = false;
                    }
                    $stale_for = $response_age - $response_global_maxage;
                    $request_max_stale = (int) $matches[1];
                    if ($stale_for < $request_max_stale) {
                        $needs_validation = false;
                    }
                }
            }
        }
        foreach ($response_directives as $response_directive) {
            if ($is_stale) {
                if ($response_directive === 'must-revalidate') {
                    $needs_validation = true;
                }
                if ($response_directive === 'proxy-revalidate') {
                    $needs_validation = true;
                }
            }
            if (!$is_stale) {
                if ($response_directive === 'immutable') {
                    $needs_validation = false;
                }
            }
        }
        return $needs_validation;
    }

    public function getValidatedResponse(RequestInterface $request, ResponseInterface $response): ?ResponseInterface
    {
        $etag = $response->getHeaderLine('ETag');
        if ($etag !== '') {
            $request = $request->withHeader('If-None-Match', $etag);
        }
        $last_modified = $response->getHeaderLine('Last-Modified');
        if ($last_modified !== '') {
            $request = $request->withHeader('If-Modified-Since', $last_modified);
        }
        $fresh_response = $this->getFreshResponse($request);
        if ($fresh_response->getStatusCode() === 304) {
            return $response
                ->withHeader('Date', date(DATE_RFC7231))
                ->withHeader('Age', '0');
        } else {
            return $fresh_response;
        }
    }

    public function responseCanBeCached(RequestInterface $request, ResponseInterface $response): bool
    {
        if (!in_array($response->getStatusCode(), [200, 301, 308, 404])) {
            return false;
        }
        $response_cache_control = $response->getHeader('Cache-Control');
        if (empty($response_cache_control)) {
            return false;
        }
        $response_directives = explode(',', implode(', ', $response_cache_control));
        $response_directives = array_map('trim', $response_directives);
        $response_directives = array_map('mb_strtolower', $response_directives);
        if (!in_array('public', $response_directives)) {
            return false;
        }
        if (in_array('no-store', $response_directives)) {
            return false;
        }
        return true;
    }

    public function cacheResponse(RequestInterface $request, ResponseInterface $response): bool
    {
        return $this->cache->set(
            $this->getCacheItemKey($request),
            [
                'status_code' => $response->getStatusCode(),
                'reason_phrase' => $response->getReasonPhrase(),
                'headers' => $response->getHeaders(),
                'body' => (string) $response->getBody(),
            ],
            null
        );
    }

}
