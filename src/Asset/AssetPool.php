<?php

/*
 * Copyright 2021 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\Component\Asset;

use Exception;

class AssetPool
{
    private $path = '';
    private $etag = '';
    private $etagFile = '';
    private $maxAge = 0;
    private $gzip = [];

    /**
     * @param array $gzip Array of MIME types that will be served gizipped.  
     */
    public function __construct(string $path, string $etag, string $etag_file, int $max_age, array $gzip)
    {
        if (!preg_match('#^[a-zA-Z0-9]*$#', $etag)) {
            throw new Exception('Etag must be empty or contain only alphanumerical characters.');
        }
        $this->path = $path;
        $this->etag = $etag;
        $this->etagFile = $etag_file;
        $this->maxAge = $max_age;
        $this->gzip = $gzip;
    }

    public function getPath(): string
    {
        return $this->path;
    }

    public function getEtag(): string
    {
        if ($this->etag !== '') {
            return $this->etag;
        }
        if ($this->etagFile !== '') {
            if (false === file_exists($this->etagFile)) {
                throw new Exception('Invalid asset pool etag file path (' . $this->etagFile . ')');
            }
            $this->etag = date('YmdHis', filemtime($this->etagFile));
        }
        return $this->etag;
    }

    public function getEtagFile(): string
    {
        return $this->etagFile;
    }

    public function getMaxAge(): int
    {
        return $this->maxAge;
    }

    public function getGzip(): array
    {
        return $this->gzip;
    }

}
