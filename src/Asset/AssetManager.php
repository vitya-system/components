<?php

/*
 * Copyright 2021 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\Component\Asset;

use InvalidArgumentException;

class AssetManager
{
    private $assetPools = [];

    public function getAssetPool(string $name): AssetPool
    {
        if (false === isset($this->assetPools[$name])) {
            throw new InvalidArgumentException('Asset pool "' . $name . '" is not defined.');
        }
        return $this->assetPools[$name];
    }

    public function addAssetPool(string $name, string $path, string $etag, string $etag_file, int $max_age, array $gzip): self
    {
        if (isset($this->assetPools[$name])) {
            throw new InvalidArgumentException('Asset pool "' . $name . '" is already defined.');
        }
        $asset_pool = new AssetPool($path, $etag, $etag_file, $max_age, $gzip);
        $this->assetPools[$name] = $asset_pool;
        return $this;
    }

}
