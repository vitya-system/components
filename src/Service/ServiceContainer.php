<?php

/*
 * Copyright 2020 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\Component\Service;

use ArrayAccess;
use ArrayIterator;
use Exception;
use IteratorAggregate;
use Psr\Container\ContainerInterface;
use TypeError;
use Vitya\Component\Service\Exception\ContainerException;
use Vitya\Component\Service\Exception\NotFoundException;

class ServiceContainer implements ArrayAccess, IteratorAggregate, ContainerInterface
{
    private $dependencyInjector = null;
    private $mutable = true;
    private $services = [];
    private $serviceProviders = [];
    private $parameters = [];

    public function __construct()
    {
        $this->dependencyInjector = new DependencyInjector($this);
    }

    public function offsetExists($offset): bool
    {
        if (!is_string($offset)) {
            throw new TypeError('Parameter offset must be a string, ' . gettype($offset) . ' given.');
        }
        return isset($this->parameters[$offset]);
    }

    public function offsetGet($offset)
    {
        if (!is_string($offset)) {
            throw new TypeError('Parameter offset must be a string, ' . gettype($offset) . ' given.');
        }
        if (!isset($this->parameters[$offset])) {
            throw new ContainerException('Undefined parameter offset (' . $offset . ').');
        }
        return $this->parameters[$offset];
    }

    public function offsetSet($offset, $value): void
    {
        if (!is_string($offset)) {
            throw new TypeError('Parameter offset must be a string, ' . gettype($offset) . ' given.');
        }
        if (!$this->mutable) {
            throw new ContainerException('This container is now immutable, you cannot change its parameters.');
        }
        $this->parameters[$offset] = $value;
    }

    public function offsetUnset($offset): void
    {
        if (!is_string($offset)) {
            throw new TypeError('Parameter offset must be a string, ' . gettype($offset) . ' given.');
        }
        unset($this->parameters[$offset]);
    }

    public function getIterator()
    {
        return new ArrayIterator($this->parameters);
    }

    public function get($id)
    {
        if (!is_string($id)) {
            throw new TypeError('Service id must be a string, ' . gettype($id) . ' given.');
        }
        if (!isset($this->services[$id])) {
            if (isset($this->serviceProviders[$id])) {
                foreach ($this->serviceProviders[$id] as $service_provider) {
                    $this->services[$id] = $service_provider->instantiateService($this);
                }
            }
        }
        if (!isset($this->services[$id])) {
            throw new NotFoundException('Service not found (' . $id . ').');
        }
        return $this->services[$id];
    }

    public function has($id)
    {
        if (!is_string($id)) {
            return false;
        }
        $id_components = explode('.', $id);
        $service_name = $id_components[0];
        return (isset($this->services[$service_name]) || isset($this->serviceProviders[$service_name]));
    }

    private function set($id, $service): self
    {
        $this->services[$id] = $service;
        return $this;
    }

    public function getDependencyInjector(): DependencyInjectorInterface
    {
        return $this->dependencyInjector;
    }

    public function getParameters(): array
    {
        return $this->parameters;
    }

    public function makeImmutable(): self
    {
        $this->mutable = false;
        return $this;
    }

    public function isMutable(): bool
    {
        return $this->mutable;
    }

    public function register(ServiceProviderInterface $service_provider): self
    {
        if (!$this->mutable) {
            throw new ContainerException('This container is now immutable, you cannot register new services.');
        }
        if (isset($this->serviceProviders[$service_provider->getName()])) {
            throw new ContainerException('Service "' . $service_provider->getName() . '" is already registered.');
        }
        $this->serviceProviders[$service_provider->getName()] = [$service_provider];
        foreach ($service_provider->getTypeHints() as $type_hint) {
            $this->dependencyInjector->setServiceMapEntry($type_hint, $service_provider->getName());
        }
        return $this;
    }

    public function extend(ServiceProviderInterface $service_provider): self
    {
        if (!$this->mutable) {
            throw new ContainerException('This container is now immutable, you cannot register new services.');
        }
        if (!isset($this->serviceProviders[$service_provider->getName()])) {
            throw new ContainerException('Service "' . $service_provider->getName() . '" is not a registered service.');
        }
        $this->serviceProviders[$service_provider->getName()][] = $service_provider;
        foreach ($service_provider->getTypeHints() as $type_hint) {
            $this->dependencyInjector->setServiceMapEntry($type_hint, $service_provider->getName());
        }
        return $this;
    }

}
