<?php

/*
 * Copyright 2020, 2022 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\Component\Service;

use Psr\Http\Message\RequestInterface;
use ReflectionMethod;
use Vitya\Component\Service\DependencyInjectorInterface;
use Vitya\Component\Service\Exception\DependencyInjectorException;
use Vitya\Component\Service\ServiceContainer;

class DependencyInjector implements DependencyInjectorInterface
{
    private $serviceContainer = null;
    private $serviceMap = [];
    private $parameterBindings = [];

    public function __construct(ServiceContainer $service_container)
    {
        $this->serviceContainer = $service_container;
    }

    public function getApp(): ServiceContainer
    {
        return $this->serviceContainer;
    }

    public function setServiceMapEntry($type_hint, $service_name)
    {
        $this->serviceMap[$type_hint] = $service_name;
    }

    public function getServiceFromHint(string $hint): ?object
    {
        if (isset($this->serviceMap[$hint])) {
            return $this->serviceContainer->get($this->serviceMap[$hint]);
        }
        return null;
    }

    public function make(string $class_name): object
    {
        if (method_exists($class_name, '__construct')) {
            $constructor_arguments = $this->getArguments(
                $class_name . '::__construct',
                []
            );
            $new_object = new $class_name(...array_values($constructor_arguments));
        } else {
            $new_object = new $class_name();
        }
        return $new_object;
    }

    public function getParameterBindings(): array
    {
        return $this->parameterBindings;
    }

    public function bindParameter(string $callable, string $param_name, string $bound_param_name): self
    {
        if (false === isset($this->parameterBindings[$callable])) {
            $this->parameterBindings[$callable] = [];
        }
        $this->parameterBindings[$callable][$param_name] = $bound_param_name;
        return $this;
    }

    public function getArguments($callable, array $additional_parameters): array
    {
        if (is_array($callable)) {
            $reflection = new ReflectionMethod(...$callable);
        } else {
            $reflection = new ReflectionMethod($callable);
        }
        $injected_parameters = [];
        foreach ($reflection->getParameters() as $param_reflection) {
            $param_name = $param_reflection->getName();
            $param_type = $param_reflection->getType();
            // Try to find an additional parameter with the same name.
            if (isset($additional_parameters[$param_name])) {
                $injected_parameters[$param_name] = $additional_parameters[$param_name];
                continue;
            }
            // Try to find a container parameter bound to this callable.
            if (is_string($callable)) {
                if (isset($this->parameterBindings[$callable]) && isset($this->parameterBindings[$callable][$param_name])) {
                    $bound_param_name = $this->parameterBindings[$callable][$param_name];
                    $injected_parameters[$param_name] = $this->serviceContainer[$bound_param_name];
                    continue;
                }
            }
            // Try to find a service matching the type hint.
            if (null !== $param_type) {
                $type_hint = $param_type->getName();
                $service = $this->getServiceFromHint($type_hint);
                if ($service !== null) {
                    $injected_parameters[$param_name] = $service;
                    continue;
                }
            }
            throw new DependencyInjectorException('Could not find a value to inject for parameter $' . $param_name . '.');
        }
        return $injected_parameters;
    }

}
