<?php

/*
 * Copyright 2021 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\Component\L10n;

use InvalidArgumentException;
use RuntimeException;

class LocaleCatalog
{
    private $locales = [];
    private $defaultLocaleId = '';

    public function getLocale(string $id): Locale
    {
        if (false === isset($this->locales[$id])) {
            throw new RuntimeException('Locale "' . $id . '" is not defined.');
        }
        return $this->locales[$id];
    }

    public function getLocales(): array
    {
        if (empty($this->locales)) {
            throw new RuntimeException('Cannot get locales: no locale is defined.');
        }
        return $this->locales;
    }

    public function getDefaultLocale(): Locale
    {
        if (empty($this->locales)) {
            throw new RuntimeException('Cannot get default locale: no locale is defined.');
        }
        return $this->locales[$this->defaultLocaleId];
    }

    public function addLocale(Locale $locale): self
    {
        if (empty($this->locales)) {
            $this->defaultLocaleId = $locale->getId();
        }
        $this->locales[$locale->getId()] = $locale;
        return $this;
    }

    public function setDefaultLocaleId(string $id): self
    {
        if (false === isset($this->locales[$id])) {
            throw new InvalidArgumentException('Locale "' . $id . '" is not defined.');
        }
        $this->defaultLocaleId = $id;
        return $this;
    }

    public function hasLocale(string $id): bool
    {
        return isset($this->locales[$id]);
    }

}
