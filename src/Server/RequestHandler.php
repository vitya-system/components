<?php

/*
 * Copyright 2020, 2024 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\Component\Server;

use Psr\EventDispatcher\EventDispatcherInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Throwable;
use Vitya\Component\Controller\Event\ControllerExceptionEvent;
use Vitya\Component\Controller\Exception\NotFoundException;
use Vitya\Component\Machine\MachineInterface;
use Vitya\Component\Route\Exception\NoRouteFoundException;
use Vitya\Component\Route\RouterInterface;

class RequestHandler implements RequestHandlerInterface
{
    private $eventDispatcher = null;
    private $machine = null;
    private $router = null;

    public function __construct(
        MachineInterface $machine,
        RouterInterface $router,
        EventDispatcherInterface $event_dispatcher
    ) {
        $this->eventDispatcher = $event_dispatcher;
        $this->machine = $machine;
        $this->router = $router;
    }

    public function getEventDispatcher(): EventDispatcherInterface
    {
        return $this->eventDispatcher;
    }

    public function getMachine(): MachineInterface
    {
        return $this->machine;
    }

    public function getRouter(): RouterInterface
    {
        return $this->router;
    }

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        // Try to get a response from the machine.
        try {
            try {
                $processing_instruction = $this->router->findProcessingInstruction($request);
            } catch (NoRouteFoundException $e) {
                throw new NotFoundException($e->getMessage());
            }
            $request = $request->withAttribute('Processing-Instruction', $processing_instruction);
            $response = $this->machine->handle($request);
        } catch (Throwable $t) {
            $event = new ControllerExceptionEvent($t);
            $this->eventDispatcher->dispatch($event);
            $response = $event->getResponse();
            if ($response !== null) {
                return $response;
            }
            throw $t;
        }
        // Fix headers.
        if (false === $response->hasHeader('Content-Type')) {
            $response = $response->withHeader('Content-Type', 'text/html; charset=UTF-8');
        }
        return $response;
    }

}
