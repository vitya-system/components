<?php

/*
 * Copyright 2020 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\Component\Server;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

class MiddlewareRequestHandler implements RequestHandlerInterface
{
    private $baseHandler = null;
    private $middleware = [];
    private $middlewareArrayIsSorted = true;

    public function __construct(RequestHandlerInterface $base_handler)
    {
        $this->baseHandler = $base_handler;
    }

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        if (!$this->middlewareArrayIsSorted) {
            arsort($this->middleware);
            $this->middlewareArrayIsSorted = true;
        }
        $current_handler = $this->baseHandler;
        foreach ($this->middleware as $priority => $middleware_group) {
            foreach ($middleware_group as $m) {
                $current_handler = new DecoratingRequestHandler($m, $current_handler);
            }
        }
        return $current_handler->handle($request);
    }

    public function addMiddleware(MiddlewareInterface $middleware, int $priority): self
    {
        if (!isset($this->middleware[$priority])) {
            $this->middleware[$priority] = [];
            $this->middlewareArrayIsSorted = false;
        }
        $this->middleware[$priority][] = $middleware;
        return $this;
    }

}
