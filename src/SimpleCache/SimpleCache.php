<?php

/*
 * Copyright 2020 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\Component\SimpleCache;

use DateInterval;
use DateTime;
use Psr\Http\Message\StreamFactoryInterface;
use Psr\Http\Message\StreamInterface;
use Traversable;
use Vitya\Component\SimpleCache\Exception\CacheException;
use Vitya\Component\SimpleCache\Exception\InvalidArgumentException;

class SimpleCache implements StreamableCacheInterface
{
    private $cacheDir = '';
    private $streamFactory = null;

    public function __construct(string $cache_dir = '', StreamFactoryInterface $stream_factory)
    {
        $this->cacheDir = $cache_dir;
        $this->streamFactory = $stream_factory;
    }

    public function get($key, $default = null)
    {
        if (!is_string($key)) {
            throw new InvalidArgumentException('Key must be a string, ' . gettype($key) . ' passed instead.');
        }
        if (!file_exists($this->getItemMetadataFilename($key)) || !file_exists($this->getItemMetadataFilename($key))) {
            return $default;
        }
        $serialized_metadata = file_get_contents($this->getItemMetadataFilename($key));
        if ($serialized_metadata === false) {
            return $default;
        }
        $metadata = unserialize($serialized_metadata);
        if (!is_array($metadata)) {
            return $default;
        }
        if ($metadata['expires_at'] !== null) {
            $expiration_ts = (int) $metadata['expires_at'];
            if ($expiration_ts < time()) {
                return $default;
            }
        }
        $content = file_get_contents($this->getItemContentFilename($key));
        if ($content === false) {
            return $default;
        }
        if ($metadata['is_string']) {
            return $content;
        }
        return unserialize($content);
    }

    public function set($key, $value, $ttl = null)
    {
        if (!is_string($key)) {
            throw new InvalidArgumentException('Key must be a string, ' . gettype($key) . ' passed instead.');
        }
        $metadata = [
            'expires_at' => null,
            'is_string' => (is_string($value)),
        ];
        if (is_int($ttl)) {
            $metadata['expires_at'] = (int) (time() + $ttl);
        } elseif ($ttl instanceof DateInterval) {
            $metadata['expires_at'] = (int) (new DateTime())->add($ttl)->format('U');
        } elseif ($ttl !== null) {
            throw new InvalidArgumentException('TTL must be expressed as null, an integer or a DateInterval object.');
        }
        $path = $this->getPath($key);
        if (!is_dir($path)) {
            mkdir($path, 0777, true);
        }
        $metadata_filename = $this->getItemMetadataFilename($key);
        $result = file_put_contents($metadata_filename, serialize($metadata));
        if ($result === false) {
            return false;
        }
        $content_filename = $this->getItemContentFilename($key);
        $result = file_put_contents($content_filename, is_string($value) ? $value : serialize($value));
        if ($result === false) {
            return false;
        }
        return true;
    }


    public function delete($key)
    {
        if (!is_string($key)) {
            throw new InvalidArgumentException('Key must be a string, ' . gettype($key) . ' passed instead.');
        }
        return unlink($this->getItemMetadataFilename($key)) && unlink($this->getItemContentFilename($key));
    }

    public function clear()
    {
        $result = true;
        foreach (glob($this->cacheDir . '/[abcdef0123456789][abcdef0123456789]') as $subdir1) {
            foreach (glob($subdir1 . '/[abcdef0123456789][abcdef0123456789]') as $subdir2) {
                foreach (glob($subdir2 . '/*.cache') as $filename) {
                    $result = $result && unlink($filename);
                }
                $result = $result && rmdir($subdir2);
            }
            $result = $result && rmdir($subdir1);
        }
        return $result;
    }

    public function getMultiple($keys, $default = null)
    {
         if(!is_array($keys) && !$keys instanceof Traversable) {
             throw new InvalidArgumentException('Keys must be provided as a traversable variable.');
         }
         $values = [];
         foreach ($keys as $key) {
             $values[$key] = $this->get($key, $default);
         }
         return $values;
    }

    public function setMultiple($values, $ttl = null)
    {
        if(!is_array($values) && !$values instanceof Traversable) {
             throw new InvalidArgumentException('Values must be provided as a traversable variable.');
         }
         $result = true;
         foreach ($values as $key => $value) {
             $result = $result && ($values[$key] = $this->set($key, $value, $ttl));
         }
         return $result;
    }

    public function deleteMultiple($keys)
    {
        if(!is_array($keys) && !$keys instanceof Traversable) {
            throw new InvalidArgumentException('Keys must be provided as a traversable variable.');
        }
        $values = [];
        $result = true;
        foreach ($keys as $key) {
            $result = $result && $this->delete($key);
        }
        return $result;
    }

    public function has($key)
    {
        return (file_exists($this->getItemMetadataFilename($key)) && file_exists($this->getItemMetadataFilename($key)));
    }

    protected function getPath($key)
    {
        if (!is_string($key)) {
            throw new InvalidArgumentException('Key must be a string, ' . gettype($key) . ' passed instead.');
        }
        $hash = sha1($key);
        $dir = '/' . substr($hash, 0, 2) . '/' . substr($hash, 2, 2);
        return $this->cacheDir . '/' . $dir;
    }

    protected function getItemMetadataFilename($key)
    {
        if (!is_string($key)) {
            throw new InvalidArgumentException('Key must be a string, ' . gettype($key) . ' passed instead.');
        }
        $hash = sha1($key);
        $filename = $hash . '.metadata.cache';
        $dir = '/' . substr($hash, 0, 2) . '/' . substr($hash, 2, 2);
        return $this->getPath($key) . '/' . $filename;
    }

    protected function getItemContentFilename($key)
    {
        if (!is_string($key)) {
            throw new InvalidArgumentException('Key must be a string, ' . gettype($key) . ' passed instead.');
        }
        $hash = sha1($key);
        $filename = $hash . '.content.cache';
        $dir = '/' . substr($hash, 0, 2) . '/' . substr($hash, 2, 2);
        return $this->getPath($key) . '/' . $filename;
    }

    public function getStream($key): ?StreamInterface
    {
        if (!is_string($key)) {
            throw new InvalidArgumentException('Key must be a string, ' . gettype($key) . ' passed instead.');
        }
        if (!file_exists($this->getItemMetadataFilename($key)) || !file_exists($this->getItemMetadataFilename($key))) {
            return null;
        }
        $serialized_metadata = file_get_contents($this->getItemMetadataFilename($key));
        if ($serialized_metadata === false) {
            return null;
        }
        $metadata = unserialize($serialized_metadata);
        if (!is_array($metadata)) {
            return null;
        }
        if ($metadata['expires_at'] !== null) {
            $expiration_ts = (int) $metadata['expires_at'];
            if ($expiration_ts < time()) {
                return null;
            }
        }
        if (!$metadata['is_string']) {
            return null;
        }
        $f = fopen($this->getItemContentFilename($key), 'r');
        if ($f === false) {
            return null;
        }
        return $this->streamFactory->createStreamFromResource($f);
    }

}
