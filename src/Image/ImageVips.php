<?php

/*
 * Copyright 2025 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\Component\Image;

use Exception;
use Jcupitt\Vips\Image as Vips_Image;
use Psr\Http\Message\StreamFactoryInterface;
use Psr\Http\Message\StreamInterface;

class ImageVips implements ImageInterface
{
    private $streamFactory = null;
    protected $width = 0;
    protected $height = 0;
    protected $imageResource = null;

    public function __construct(StreamFactoryInterface $stream_factory)
    {
        $this->streamFactory = $stream_factory;
    }

    public function getStreamFactory(): StreamFactoryInterface
    {
        return $this->streamFactory;
    }

    public function getWidth(): int
    {
        return $this->width;
    }

    public function getHeight(): int
    {
        return $this->height;
    }

    public function getImageResource(): ?Vips_Image
    {
        return $this->imageResource;
    }

    public function hasSmartCropFeature(): bool
    {
        return false;
    }

    public function create(int $width, int $height): static
    {
        if (0 >= $width || 0 >= $height) {
            throw new Exception('Width and height must be positive integers.');
        }
        $this->imageResource = Vips_Image::black($width, $height);
        $this->width = $width;
        $this->height = $height;
        return $this;
    }

    public function loadFromLocalFile(string $path): static
    {
        $this->imageResource = Vips_Image::newFromFile($path, []);
        $this->width = $this->imageResource->width;
        $this->height = $this->imageResource->height;
        return $this;
    }

    public function resize(int $new_width, int $new_height): static
    {
        if (0 >= $new_width && 0 >= $new_height) {
            throw new Exception('You need to specify at least a width or a height.');
        }
        if (0 >= $new_width) {
            $new_width = (int) ((float) $new_height * (float) $this->getWidth() / (float) $this->getHeight());
        }
        if (0 >= $new_height) {
            $new_height = (int) ((float) $new_width * (float) $this->getHeight() / (float) $this->getWidth());
        }
        $scale = (float) $new_width / (float) $this->getWidth();
        $vscale = (float) $new_height / (float) $this->getHeight();
        $this->imageResource = $this->getImageResource()->resize($scale, ['vscale' => $vscale, 'kernel' => 'lanczos3']);
        $this->width = $this->imageResource->width;
        $this->height = $this->imageResource->height;
        return $this;
    }

    public function loadAndResize(string $path, int $new_width, int $new_height): static
    {
        if (0 >= $new_width && 0 >= $new_height) {
            throw new Exception('You need to specify at least a width or a height.');
        }
        if (0 >= $new_width) {
            $this->imageResource = Vips_Image::thumbnail($path, 65500, ['height' => $new_height])->copyMemory();
        } elseif (0 >= $new_height) {
            $this->imageResource = Vips_Image::thumbnail($path, $new_width, ['height' => 65500])->copyMemory();
        } else {
            $this->imageResource = Vips_Image::thumbnail($path, $new_width, ['height' => $new_height, 'size' => 'force'])->copyMemory();
        }
        $this->width = $this->imageResource->width;
        $this->height = $this->imageResource->height;
        return $this;
    }

    public function crop(int $new_width, int $new_height, int $x, int $y): static
    {
        if (0 > $x || 0 > $y) {
            throw new Exception('Crop coordinates must be positive.');
        }
        if (0 >= $new_width || 0 >= $new_height) {
            throw new Exception('Width and height must be positive integers.');
        }
        if ($this->getWidth() < $x + $new_width) {
            $new_width = $this->getWidth() - $x;
        }
        if ($this->getHeight() < $y + $new_height) {
            $new_height = $this->getHeight() - $y;
        }
        $this->imageResource = $this->imageResource->crop($x, $y, $new_width, $new_height);
        $this->width = $this->imageResource->width;
        $this->height = $this->imageResource->height;
        return $this;
    }

    public function autocrop(int $new_width, int $new_height): static
    {
        if ($this->getWidth() < $new_width) {
            $new_width = $this->getWidth();
        }
        if ($this->getHeight() < $new_height) {
            $new_height = $this->getHeight();
        }
        $this->imageResource = $this->imageResource->smartcrop($new_width, $new_height, ['interesting' => 'attention']);
        $this->width = $this->imageResource->width;
        $this->height = $this->imageResource->height;
        return $this;
    }

    public function getSupportedOutputFileFormats(): array
    {
        return [
            'image/avif' => [
                'mime_type' => 'image/avif',
                'filename_extension' => 'avif',
            ],
            'image/jpeg' => [
                'mime_type' => 'image/jpeg',
                'filename_extension' => 'jpeg',
            ],
            'image/png' => [
                'mime_type' => 'image/png',
                'filename_extension' => 'png',
            ],
            'image/webp' => [
                'mime_type' => 'image/webp',
                'filename_extension' => 'webp',
            ],
        ];
    }

    public function output(string $mime_type, array $output_options = []): StreamInterface
    {
        // Default output options.
        $jpeg_interlace = true;
        $jpeg_quality = 75;
        $png_compression = 9;
        $png_interlace = true;
        $webp_lossless = false;
        $webp_quality = 75;
        $avif_lossless = false;
        $avif_quality = 30;
        // Parse output options.
        if (isset($output_options['jpeg_interlace']) && is_bool($output_options['jpeg_interlace'])) {
            $jpeg_interlace = $output_options['jpeg_interlace'];
        }
        if (isset($output_options['jpeg_quality']) && is_int($output_options['jpeg_quality'])) {
            if (0 <= $output_options['jpeg_quality'] && 100 >= $output_options['jpeg_quality']) {
                $jpeg_quality = $output_options['jpeg_quality'];
            }
        }
        if (isset($output_options['png_compression']) && is_int($output_options['png_compression'])) {
            if (0 <= $output_options['png_compression'] && 9 >= $output_options['png_compression']) {
                $png_compression = $output_options['png_compression'];
            }
        }
        if (isset($output_options['png_interlace']) && is_bool($output_options['png_interlace'])) {
            $png_interlace = $output_options['png_interlace'];
        }
        if (isset($output_options['webp_lossless']) && is_bool($output_options['webp_lossless'])) {
            $webp_lossless = $output_options['webp_lossless'];
        }
        if (isset($output_options['webp_quality']) && is_int($output_options['webp_quality'])) {
            if (0 <= $output_options['webp_quality'] && 100 >= $output_options['webp_quality']) {
                $webp_quality = $output_options['webp_quality'];
            }
        }
        if (isset($output_options['avif_lossless']) && is_bool($output_options['avif_lossless'])) {
            $avif_lossless = $output_options['avif_lossless'];
        }
        if (isset($output_options['avif_quality']) && is_int($output_options['avif_quality'])) {
            if (0 <= $output_options['avif_quality'] && 100 >= $output_options['avif_quality']) {
                $avif_quality = $output_options['avif_quality'];
            }
        }
        // Output.
        if ('image/avif' === $mime_type) {
            $options_string_elements = [];
            if ($avif_lossless) {
                $options_string_elements[] = 'lossless=true';
            } else {
                $options_string_elements[] = 'Q=' . $avif_quality;
            }
            $buffer = $this->getImageResource()->writeToBuffer('.avif[' . implode(',', $options_string_elements) . ']');
            return $this->getStreamFactory()->createStream($buffer);
        } elseif ('image/jpeg' === $mime_type) {
            $options_string_elements = [
                'Q=' . $jpeg_quality,
                'interlace=' . ($jpeg_interlace ? 'true' : 'false'),
            ];
            $buffer = $this->getImageResource()->writeToBuffer('.jpeg[' . implode(',', $options_string_elements) . ']');
            return $this->getStreamFactory()->createStream($buffer);
        } elseif ('image/png' === $mime_type) {
            $options_string_elements = [
                'compression=' . $png_compression,
                'interlace=' . ($png_interlace ? 'true' : 'false'),
            ];
            $buffer = $this->getImageResource()->writeToBuffer('.png[' . implode(',', $options_string_elements) . ']');
            return $this->getStreamFactory()->createStream($buffer);
        } elseif ('image/webp' === $mime_type) {
            if ($webp_lossless) {
                $options_string_elements[] = 'lossless=true';
            } else {
                $options_string_elements[] = 'Q=' . $webp_quality;
            }
            $buffer = $this->getImageResource()->writeToBuffer('.webp[' . implode(',', $options_string_elements) . ']');
            return $this->getStreamFactory()->createStream($buffer);
        }
        throw new Exception('Unknown output MIME type: "' . $mime_type . '".');
    }

}
