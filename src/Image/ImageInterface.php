<?php

/*
 * Copyright 2025 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\Component\Image;

use Psr\Http\Message\StreamInterface;

interface ImageInterface
{
    public function getWidth(): int;

    public function getHeight(): int;

    public function hasSmartCropFeature(): bool;

    public function create(int $width, int $height): static;

    public function loadFromLocalFile(string $path): static;

    public function resize(int $new_width, int $new_height): static;

    public function loadAndResize(string $path, int $new_width, int $new_height): static;

    public function crop(int $new_width, int $new_height, int $x, int $y): static;

    public function autocrop(int $new_width, int $new_height): static;

    public function getSupportedOutputFileFormats(): array;

    public function output(string $mime_type, array $output_options = []): StreamInterface;

}
