<?php

/*
 * Copyright 2025 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\Component\Image;

use Exception;
use Psr\Http\Message\StreamFactoryInterface;

class ImageFactory implements ImageFactoryInterface
{
    private $backend = '';
    private $streamFactory = null;

    public function __construct(string $backend, StreamFactoryInterface $stream_factory)
    {
        $this->backend = $backend;
        $this->streamFactory = $stream_factory;
    }

    public function getBackend(): string
    {
        return $this->backend;
    }

    public function getStreamFactory(): StreamFactoryInterface
    {
        return $this->streamFactory;
    } 

    public function makeImage(): ImageInterface
    {
        if ('gd2' === $this->backend) {
            $image = new ImageGd2($this->getStreamFactory());
        } elseif ('vips' === $this->backend) {
            $image = new ImageVips($this->getStreamFactory());
        } else {
            throw new Exception('Unknown imaga backend: "' . $this->backend . '".');
        }
        return $image;
    }

}
