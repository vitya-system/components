<?php

/*
 * Copyright 2025 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\Component\Image;

use Exception;
use GdImage;
use Psr\Http\Message\StreamFactoryInterface;
use Psr\Http\Message\StreamInterface;

class ImageGd2 implements ImageInterface
{
    private $streamFactory = null;
    private $width = 0;
    private $height = 0;
    private $imageResource = null;

    public function __construct(StreamFactoryInterface $stream_factory)
    {
        $this->streamFactory = $stream_factory;
    }

    public function getStreamFactory(): StreamFactoryInterface
    {
        return $this->streamFactory;
    }

    public function getWidth(): int
    {
        return $this->width;
    }

    public function getHeight(): int
    {
        return $this->height;
    }

    public function getImageResource(): ?GdImage
    {
        return $this->imageResource;
    }

    public function hasSmartCropFeature(): bool
    {
        return false;
    }

    public function create(int $width, int $height): static
    {
        if (0 >= $width || 0 >= $height) {
            throw new Exception('Width and height must be positive integers.');
        }
        $this->imageResource = imagecreatetruecolor($width, $height);
        $this->width = $width;
        $this->height = $height;
        return $this;
    }

    public function loadFromLocalFile(string $path): static
    {
        $this->imageResource = @imagecreatefromstring(@file_get_contents($path));
        if ($this->imageResource == false) {
            throw new Exception('Invalid source image file.');
        }
        $exif = exif_read_data($path);
        if (false !== $exif && isset($exif['Orientation'])) {
            $orientation = $exif['Orientation'];
            switch ($orientation) {
                case 2:
                    imageflip($this->imageResource, IMG_FLIP_HORIZONTAL);
                    break;
                case 3:
                    $this->imageResource = imagerotate($this->imageResource, 180, 0);
                    break;
                case 4:
                    imageflip($this->imageResource, IMG_FLIP_VERTICAL);
                    break;
                case 5:
                    imageflip($this->imageResource, IMG_FLIP_VERTICAL);
                    $this->imageResource = imagerotate($this->imageResource, 270, 0);
                    break;
                case 6:
                    $this->imageResource = imagerotate($this->imageResource, 270, 0);
                    break;
                case 7:
                    imageflip($this->imageResource, IMG_FLIP_VERTICAL);
                    $this->imageResource = imagerotate($this->imageResource, 90, 0);
                    break;
                case 8:
                    $this->imageResource = imagerotate($this->imageResource, 90, 0);
                    break;
            }
        }
        $this->width = (int) imagesx($this->imageResource);
        $this->height = (int) imagesy($this->imageResource);
        return $this;
    }

    public function resize(int $new_width, int $new_height): static
    {
        if (0 >= $new_width && 0 >= $new_height) {
            throw new Exception('You need to specify at least a width or a height.');
        }
        if (0 >= $new_width) {
            $new_width = (int) ((float) $new_height * (float) $this->getWidth() / (float) $this->getHeight());
        }
        if (0 >= $new_height) {
            $new_height = (int) ((float) $new_width * (float) $this->getHeight() / (float) $this->getWidth());
        }
        $new_image_resource = imagecreatetruecolor($new_width, $new_height);
        imagealphablending($new_image_resource, false);
        $transparent_color = imagecolorallocatealpha($new_image_resource, 0, 0, 0, 127);
        imagefilledrectangle($new_image_resource, 0, 0, $new_width, $new_height, $transparent_color);
        imagealphablending($new_image_resource, true);
        imagecopyresampled($new_image_resource, $this->getImageResource(), 0, 0, 0, 0, $new_width, $new_height, $this->width, $this->height);
        $this->imageResource = $new_image_resource;
        $this->width = $new_width;
        $this->height = $new_height;
        return $this;
    }

    public function loadAndResize(string $path, int $new_width, int $new_height): static
    {
        return $this->loadFromLocalFile($path)->resize($new_width, $new_height);
    }

    public function crop(int $new_width, int $new_height, int $x, int $y): static
    {
        if (0 > $x || 0 > $y) {
            throw new Exception('Crop coordinates must be positive.');
        }
        if (0 >= $new_width || 0 >= $new_height) {
            throw new Exception('Width and height must be positive integers.');
        }
        if ($this->getWidth() < $x + $new_width) {
            $new_width = $this->getWidth() - $x;
        }
        if ($this->getHeight() < $y + $new_height) {
            $new_height = $this->getHeight() - $y;
        }
        $new_image_resource = imagecreatetruecolor($new_width, $new_height);
        imagealphablending($new_image_resource, false);
        $transparent_color = imagecolorallocatealpha($new_image_resource, 255, 255, 255, 127);
        imagefilledrectangle($new_image_resource, 0, 0, $new_width, $new_height, $transparent_color);
        imagealphablending($new_image_resource, true);
        imagecopy($new_image_resource, $this->imageResource, 0, 0, $x, $y, $new_width, $new_height);
        $this->imageResource = $new_image_resource;
        $this->width = $new_width;
        $this->height = $new_height;
        return $this;
    }

    public function autocrop(int $new_width, int $new_height): static
    {
        $x = (int) ((float) ($this->getWidth() - $new_width) / 2.0);
        $x = (int) max($x, 0);
        $y = (int) ((float) ($this->getHeight() - $new_height) / 2.0);
        $y = (int) max($y, 0);
        $this->crop($new_width, $new_height, $x, $y);
        return $this;
    }

    public function getSupportedOutputFileFormats(): array
    {
        return [
            'image/avif' => [
                'mime_type' => 'image/avif',
                'filename_extension' => 'avif',
            ],
            'image/jpeg' => [
                'mime_type' => 'image/jpeg',
                'filename_extension' => 'jpeg',
            ],
            'image/png' => [
                'mime_type' => 'image/png',
                'filename_extension' => 'png',
            ],
            'image/webp' => [
                'mime_type' => 'image/webp',
                'filename_extension' => 'webp',
            ],
        ];
    }

    public function output(string $mime_type, array $output_options = []): StreamInterface
    {
        // Default output options.
        $jpeg_interlace = true;
        $jpeg_quality = 75;
        $png_compression = 9;
        $webp_quality = 75;
        $avif_quality = 30;
        // Parse output options.
        if (isset($output_options['jpeg_interlace']) && is_bool($output_options['jpeg_interlace'])) {
            $jpeg_interlace = $output_options['jpeg_interlace'];
        }
        if (isset($output_options['jpeg_quality']) && is_int($output_options['jpeg_quality'])) {
            if (0 <= $output_options['jpeg_quality'] && 100 >= $output_options['jpeg_quality']) {
                $jpeg_quality = $output_options['jpeg_quality'];
            }
        }
        if (isset($output_options['png_compression']) && is_int($output_options['png_compression'])) {
            if (0 <= $output_options['png_compression'] && 9 >= $output_options['png_compression']) {
                $png_compression = $output_options['png_compression'];
            }
        }
        if (isset($output_options['webp_quality']) && is_int($output_options['webp_quality'])) {
            if (0 <= $output_options['webp_quality'] && 100 >= $output_options['webp_quality']) {
                $webp_quality = $output_options['webp_quality'];
            }
        }
        if (isset($output_options['avif_quality']) && is_int($output_options['avif_quality'])) {
            if (0 <= $output_options['avif_quality'] && 100 >= $output_options['avif_quality']) {
                $avif_quality = $output_options['avif_quality'];
            }
        }
        // Output.
        if ('image/avif' === $mime_type) {
            ob_start();
            imagesavealpha($this->imageResource, true);
            imageavif($this->imageResource, null, $avif_quality);
            $buffer = ob_get_clean();
            return $this->getStreamFactory()->createStream($buffer);
        } elseif ('image/jpeg' === $mime_type) {
            ob_start();
            imageinterlace($this->imageResource, $jpeg_interlace);
            imagejpeg($this->imageResource, null, $jpeg_quality);
            $buffer = ob_get_clean();
            return $this->getStreamFactory()->createStream($buffer);
        } elseif ('image/png' === $mime_type) {
            ob_start();
            imagesavealpha($this->imageResource, true);
            imagepng($this->imageResource, null, $png_compression);
            $buffer = ob_get_clean();
            return $this->getStreamFactory()->createStream($buffer);
        } elseif ('image/webp' === $mime_type) {
            ob_start();
            imagesavealpha($this->imageResource, true);
            imagewebp($this->imageResource, null, $webp_quality);
            $buffer = ob_get_clean();
            return $this->getStreamFactory()->createStream($buffer);
        }
        throw new Exception('Unknown output MIME type: "' . $mime_type . '".');
    }

}
