<?php

/*
 * Copyright 2020 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\Component\Frontend;

use Vitya\Component\Command\CommandInterface;
use Vitya\Component\Command\Terminal;

class CliFrontend
{
    private $commands = [];

    public function addCommand(CommandInterface $command)
    {
        $this->commands[$command->getName()] = $command;
    }

    public function run(): int
    {
        global $argc;
        global $argv;
        $io = new Terminal();
        if ($argc < 2) {
            $io->error('Please specify a command name.');
            $io->nl();
            exit (1);
        }
        $command_name = $argv[1];
        if (!isset($this->commands[$command_name])) {
            $io->error('Unknown command: "' . $command_name . '".');
            $io->nl();
            exit (1);
        }
        $exit_code = $this->commands[$command_name]->execute($io);
        exit ($exit_code);
    }

}
