<?php

/*
 * Copyright 2020, 2024 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\Component\Frontend;

use Exception;
use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestFactoryInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\StreamFactoryInterface;
use Psr\Http\Message\UploadedFileFactoryInterface;
use Psr\Http\Message\UploadedFileInterface;
use Psr\Http\Message\UriFactoryInterface;
use Psr\Http\Message\UriInterface;
use Psr\Http\Server\RequestHandlerInterface;

class WebFrontend
{
    private $requestHandler = null;
    private $mainServerRequest = null;
    private $responseFactory = null;
    private $serverRequestFactory = null;
    private $streamFactory = null;
    private $uploadedFileFactory = null;
    private $uriFactory = null;
    private $forcedDomainName = '';

    public function __construct(
        RequestHandlerInterface $request_handler,
        ResponseFactoryInterface $response_factory,
        ServerRequestFactoryInterface $server_request_factory,
        StreamFactoryInterface $stream_factory,
        UploadedFileFactoryInterface $uploaded_file_factory,
        UriFactoryInterface $uri_factory,
        string $forced_domain_name
    ) {
        $this->requestHandler = $request_handler;
        $this->responseFactory = $response_factory;
        $this->serverRequestFactory = $server_request_factory;
        $this->streamFactory = $stream_factory;
        $this->uploadedFileFactory = $uploaded_file_factory;
        $this->uriFactory = $uri_factory;
        $this->forcedDomainName = $forced_domain_name;
    }

    public function getRequestHandler(): RequestHandlerInterface
    {
        return $this->requestHandler;
    }

    public function getResponseFactory(): ResponseFactoryInterface
    {
        return $this->responseFactory;
    }

    public function getServerRequestFactory(): ServerRequestFactoryInterface
    {
        return $this->serverRequestFactory;
    }

    public function getStreamFactory(): StreamFactoryInterface
    {
        return $this->streamFactory;
    }

    public function getUploadedFileFactory(): UploadedFileFactoryInterface
    {
        return $this->uploadedFileFactory;
    }

    public function getUriFactory(): UriFactoryInterface
    {
        return $this->uriFactory;
    }

    public function getMainServerRequest(): ?ServerRequestInterface
    {
        return $this->mainServerRequest;
    }

    public function getForcedDomainName(): string
    {
        return $this->forcedDomainName;
    }

    public function createMainRequestFromSuperglobals(): ServerRequestInterface
    {
        $method = isset($_SERVER['REQUEST_METHOD']) ? $_SERVER['REQUEST_METHOD'] : 'GET';
        $headers = getallheaders();
        $uri = $this->getUriFromSuperglobals();
        $body = $this->streamFactory->createStreamFromFile('php://input', 'r');
        $protocol_version = isset($_SERVER['SERVER_PROTOCOL']) ? str_replace('HTTP/', '', $_SERVER['SERVER_PROTOCOL']) : '1.1';
        $server_request = $this->serverRequestFactory->createServerRequest($method, $uri, $_SERVER);
        foreach ($headers as $header => $value) {
            $server_request = $server_request->withHeader($header, $value);
        }
        $this->mainServerRequest = $server_request
            ->withProtocolVersion($protocol_version)
            ->withCookieParams($_COOKIE)
            ->withQueryParams($_GET)
            ->withParsedBody($_POST)
            ->withUploadedFiles($this->normalizeFiles($_FILES))
            ->withBody($body);
        return $this->mainServerRequest;
    }

    public function normalizeFiles(array $files): array
    {
        $normalized = [];
        foreach ($files as $key => $value) {
            if ($value instanceof UploadedFileInterface) {
                $normalized[$key] = $value;
            } elseif (is_array($value) && isset($value['tmp_name'])) {
                $normalized[$key] = $this->createUploadedFileFromArray($value);
            } elseif (is_array($value)) {
                $normalized[$key] = $this->normalizeFiles($value);
                continue;
            } else {
                throw new Exception('Invalid value in files specification.');
            }
        }
        return $normalized;
    }

    private function createUploadedFileFromArray(array $value): UploadedFileInterface
    {
        if (is_array($value['tmp_name'])) {
            return $this->normalizeNestedFileSpec($value);
        }
        if (is_uploaded_file($value['tmp_name'])) {
            $stream = $this->streamFactory->createStreamFromFile($value['tmp_name']);
        } else {
            $stream = $this->streamFactory->createStream();
        }
        return $this->uploadedFileFactory->createUploadedFile(
            $stream,
            (int) $value['size'],
            (int) $value['error'],
            $value['name'],
            $value['type']
        );
    }

    private function normalizeNestedFileSpec(array $files = []): array
    {
        $normalized_files = [];
        foreach (array_keys($files['tmp_name']) as $key) {
            $spec = [
                'tmp_name' => $files['tmp_name'][$key],
                'size' => $files['size'][$key],
                'error' => $files['error'][$key],
                'name' => $files['name'][$key],
                'type' => $files['type'][$key],
            ];
            $normalized_files[$key] = $this->createUploadedFileFromArray($spec);
        }
        return $normalized_files;
    }

    private function extractHostAndPortFromAuthority(string $authority): array
    {
        $uri = 'http://' . $authority;
        $parts = parse_url($uri);
        if ($parts === false) {
            return [null, null];
        }
        $host = isset($parts['host']) ? $parts['host'] : null;
        $port = isset($parts['port']) ? $parts['port'] : null;
        return [$host, $port];
    }

    public function getUriFromSuperglobals(): UriInterface
    {
        $uri = $this->uriFactory->createUri('');
        $uri = $uri->withScheme(!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' ? 'https' : 'http');
        $has_port = false;
        if (isset($_SERVER['HTTP_HOST'])) {
            list($host, $port) = $this->extractHostAndPortFromAuthority($_SERVER['HTTP_HOST']);
            if ($host !== null) {
                $uri = $uri->withHost($host);
            }
            if ($port !== null) {
                $has_port = true;
                $uri = $uri->withPort($port);
            }
        } elseif (isset($_SERVER['SERVER_NAME'])) {
            $uri = $uri->withHost($_SERVER['SERVER_NAME']);
        } elseif (isset($_SERVER['SERVER_ADDR'])) {
            $uri = $uri->withHost($_SERVER['SERVER_ADDR']);
        }
        if (!$has_port && isset($_SERVER['SERVER_PORT'])) {
            $uri = $uri->withPort((int) $_SERVER['SERVER_PORT']);
        }
        $has_query = false;
        if (isset($_SERVER['REQUEST_URI'])) {
            $request_uri_parts = explode('?', $_SERVER['REQUEST_URI'], 2);
            $uri = $uri->withPath($request_uri_parts[0]);
            if (isset($request_uri_parts[1])) {
                $has_query = true;
                $uri = $uri->withQuery($request_uri_parts[1]);
            }
        }
        if (!$has_query && isset($_SERVER['QUERY_STRING'])) {
            $uri = $uri->withQuery($_SERVER['QUERY_STRING']);
        }
        return $uri;
    }

    public function outputResponse(ResponseInterface $response): void
    {
        http_response_code($response->getStatusCode());
        $headers = $response->getHeaders();
        foreach ($headers as $k => $v) {
            if (strtolower($k) === 'set-cookie') {
                // The Set-Cookie headers cannot be folded into a single line.
                foreach ($v as $set_cookie_line) {
                    header($k . ': ' . $set_cookie_line, false);
                }
            } else {
                header($k . ': ' . $response->getHeaderLine($k));
            }
        }
        $buffer_size = 16 * 1024;
        if ($response->getBody()->isSeekable()) {
            $response->getBody()->rewind();
        }
        while (!$response->getBody()->eof()) {
            echo $response->getBody()->read($buffer_size);
        }
    }

    public function run(): void
    {
        $request = $this->createMainRequestFromSuperglobals();
        if ('' !== $this->forcedDomainName && $request->getUri()->getHost() !== $this->forcedDomainName) {
            $response = $this->responseFactory->createResponse(308)
                ->withAddedHeader('Location', (string) $request->getUri()->withHost($this->forcedDomainName))
            ;
        } else {
            $response = $this->requestHandler->handle($request);
        }
        $this->outputResponse($response);
    }

}
