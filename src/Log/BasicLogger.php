<?php

/*
 * Copyright 2024 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\Component\Log;

use Monolog\Handler\RotatingFileHandler;
use Monolog\Handler\StreamHandler;
use Monolog\Level;
use Monolog\Logger;
use Psr\Log\LoggerInterface;
use Psr\Log\LogLevel;

class BasicLogger implements BrowsableLogInterface, LoggerInterface
{
    private $monolog = null;
    private $path = '';

    public function __construct(string $name, string $path, Level $level, bool $rotate)
    {
        $this->monolog = new Logger($name);
        $this->path = $path;
        if ($rotate) {
            $this->monolog->pushHandler(new RotatingFileHandler($this->path, 30, $level));
        } else {
            $this->monolog->pushHandler(new StreamHandler($this->path, $level));
        }
    }

    public function getMonolog(): Logger
    {
        return $this->monolog;
    }

    public function getPath(): string
    {
        return $this->path;
    }

    public function emergency(string|\Stringable $message, array $context = []): void
    {
        $this->monolog->emergency($message, $context);
    }

    public function alert(string|\Stringable $message, array $context = []): void
    {
        $this->monolog->alert($message, $context);
    }

    public function critical(string|\Stringable $message, array $context = []): void
    {
        $this->monolog->critical($message, $context);
    }

    public function error(string|\Stringable $message, array $context = []): void
    {
        $this->monolog->error($message, $context);
    }
    
    public function warning(string|\Stringable $message, array $context = []): void
    {
        $this->monolog->warning($message, $context);
    }

    public function notice(string|\Stringable $message, array $context = []): void
    {
        $this->monolog->notice($message, $context);
    }

    public function info(string|\Stringable $message, array $context = []): void
    {
        $this->monolog->info($message, $context);
    }

    public function debug(string|\Stringable $message, array $context = []): void
    {
        $this->monolog->debug($message, $context);
    }

    public function log($level, string|\Stringable $message, array $context = []): void
    {
        $this->monolog->log($level, $message, $context);
    }

    public function getLines(int $first_line, int $nb_lines): array
    {
        $a = [];
        $log_handler = $this->monolog->getHandlers()[0];
        $handle = fopen($log_handler->getUrl(), 'r');
        if ($handle) {
            $current_line = -1;
            $nb_lines_read = 0;
            while (false !== ($line = fgets($handle))) {
                $current_line++;
                if ($current_line < $first_line) {
                    continue;
                }
                $level = LogLevel::INFO;
                $line_elements = explode(' ', $line);
                if (count($line_elements) > 1) {
                    $level_element = $line_elements[1];
                    preg_match('/\.([A-Z]+):/', $level_element, $matches);
                    if (isset($matches[1])) {
                        $level = strtolower($matches[1]);
                    }
                }
                $a[] = [
                    'level' => $level,
                    'content' => $line,
                ];
                $nb_lines_read++;
                if ($nb_lines_read >= $nb_lines) {
                    break;
                }
            }
            fclose($handle);
        }
        return $a;
    }

    public function logIsEmpty(): bool
    {
        $log_handler = $this->monolog->getHandlers()[0];
        if (false === file_exists($log_handler->getUrl())) {
            return true;
        }
        if (filesize($log_handler->getUrl()) === 0) {
            return true;
        }
        return false;
    }

}
