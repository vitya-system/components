<?php

/*
 * Copyright 2024 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\Component\Log;

use Exception;
use Monolog\Level;
use Psr\Log\LoggerInterface;

class LoggerCollection implements LoggerCollectionInterface
{
    private $collection = [];
    private $defaultLogDir = '';
    private $defaultLogger = '';

    public function __construct(string $default_log_dir, string $default_logger)
    {
        $this->defaultLogDir = $default_log_dir;
        $this->defaultLogger = $default_logger;
    }

    public function getDefaultLogDir(): string
    {
        return $this->defaultLogDir;
    }

    public function getDefaultLogger(): string
    {
        return $this->defaultLogger;
    }

    public function getLogger(string $name = null): LoggerInterface
    {
        if (empty($this->collection)) {
            throw new Exception('The logger collection is empty.');
        }
        if ($name === null) {
            $name = $this->defaultLogger;
        }
        if (false === isset($this->collection[$name])) {
            throw new Exception('No "' . $name . '" key defined in the logger collection.');
        }
        if ($this->collection[$name]['logger'] === null) {
            $this->collection[$name]['logger'] = $this->createLogger(
                $name, 
                $this->collection[$name]['type'],
                $this->collection[$name]['options']
            );
        }
        return $this->collection[$name]['logger'];
    }

    public function add(string $name, string $type, array $options): self
    {
        $this->collection[$name] = [
            'type' => $type,
            'options' => $options,
            'logger' => null,
        ];
        return $this;
    }

    public function has(string $name): bool
    {
        return isset($this->collection[$name]);
    }

    public function getAvailableLoggers(): array
    {
        return array_keys($this->collection);
    }

    public function createLogger(string $name, string $type, array $options): LoggerInterface
    {
        if ('basic' === $type) {
            if (false === isset($options['path']) || false === is_string($options['path'])) {
                throw new Exception('A path must be specified for BasicLogger.');
            }
            $sanitized_path = str_replace('{log_dir}', $this->getDefaultLogDir(), $options['path']);
            if (false === isset($options['rotate']) || false === is_bool($options['rotate'])) {
                throw new Exception('A rotate option must be specified for BasicLogger.');
            }
            $debug = false;
            if (isset($options['debug'])) {
                if (false === is_bool($options['debug'])) {
                    throw new Exception('The "debug" option must be a boolean.');
                }
                $debug = $options['debug'];
            }
            $level = Level::Info;
            if ($debug) {
                $level = Level::Debug;
            }
            return new BasicLogger($name, $sanitized_path, $level, $options['rotate']);
        }
    }

}
