<?php

/*
 * Copyright 2022 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\Component\FileStorage;

use Exception;
use Psr\Http\Message\UploadedFileInterface;
use Psr\Http\Message\UriFactoryInterface;
use Psr\Http\Message\UriInterface;

class LocalFileStorage implements LocalFileStorageInterface
{
    private $baseDir = '';
    private $uriFactory = null;

    public function __construct(string $base_dir, UriFactoryInterface $uri_factory)
    {
        $this->baseDir = $base_dir;
        $this->uriFactory = $uri_factory;
    }   

    public function getBaseDir(): string
    {
        return $this->baseDir;
    }

    public function getUriFactory(): UriFactoryInterface
    {
        return $this->uriFactory;
    }
    
    public function copy(string $source_path, string $target_path): bool
    {
        return copy($this->baseDir . $source_path, $this->baseDir . $target_path);
    }

    public function delete(string $path)
    {
        unlink($this->baseDir . $path);
    }

    public function fileExists(string $path): bool
    {
        return file_exists($this->baseDir . $path);
    }

    public function isFile(string $path): bool
    {
        return is_file($this->baseDir . $path);
    }

    public function isDir(string $path): bool
    {
        return is_dir($this->baseDir . $path);
    }

    public function createDir(string $path): bool
    {
        return mkdir($this->baseDir . $path, 0777, true);
    }

    public function getSize(string $path): ?int
    {
        $size = filesize($this->baseDir . $path);
        return (false === $size ? null : $size);
    }

    public function moveUploadedFile(UploadedFileInterface $uploaded_file, string $target_path)
    {
        $uploaded_file->moveTo($this->baseDir . $target_path);
    }

    public function copyLocalFile(string $local_path, string $target_path)
    {
        if (false === copy($local_path, $this->baseDir . $target_path)) {
            throw new Exception('Could not copy local file ' . $local_path . ' to destination path ' . $this->baseDir . $target_path . '.'); 
        }
    }

    public function getLocalFilesystemPath(string $path): ?string
    {
        return ($this->fileExists($path) ? $this->baseDir . $path : null);
    }

    public function getPublicUri(string $path): ?UriInterface
    {
        return null;
    }

}
