<?php

/*
 * Copyright 2022 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\Component\FileStorage;

use Psr\Http\Message\UploadedFileInterface;
use Psr\Http\Message\UriInterface;

interface LocalFileStorageInterface
{
    public function copy(string $source_path, string $target_path): bool;

    public function delete(string $path);

    public function fileExists(string $path): bool;

    public function isFile(string $path): bool;

    public function isDir(string $path): bool;

    public function createDir(string $path): bool;

    public function getSize(string $path): ?int;

    public function copyLocalFile(string $local_path, string $target_path);

    public function moveUploadedFile(UploadedFileInterface $uploaded_file, string $target_path);

    public function getLocalFilesystemPath(string $path): ?string;

    public function getPublicUri(string $path): ?UriInterface;

}
