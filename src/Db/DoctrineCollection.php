<?php

/*
 * Copyright 2020 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\Component\Db;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\DriverManager;
use Doctrine\DBAL\Tools\DsnParser;
use Exception;

class DoctrineCollection
{
    private $collection = [];

    public function getConnection(string $name = null): Connection
    {
        if (empty($this->collection)) {
            throw new Exception('The Doctrine collection is empty.');
        }
        if ($name === null) {
            $name = array_keys($this->collection)[0];
        } elseif (false === isset($this->collection[$name])) {
            throw new Exception('No "' . $name . '" key defined in the Doctrine collection.');
        }
        if ($this->collection[$name]['connection'] === null) {
            $dsn_parser = new DsnParser();
            $connection_params = $dsn_parser->parse($this->collection[$name]['url']);
            $this->collection[$name]['connection'] = DriverManager::getConnection($connection_params);
        }
        return $this->collection[$name]['connection'];
    }

    public function add(string $name, string $url): self
    {
        $this->collection[$name] = [
            'url' => $url,
            'connection' => null,
        ];
        return $this;
    }

    public function has(string $name): bool
    {
        return isset($this->collection[$name]);
    }

}
