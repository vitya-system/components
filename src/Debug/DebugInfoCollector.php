<?php

/*
 * Copyright 2020 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\Component\Debug;

use InvalidArgumentException;

class DebugInfoCollector
{
    private $collection = [];

    public function getCollection(): array
    {
        return $this->collection;
    }

    public function add($item): self
    {
        $this->assertItemIsValid($item);
        $this->collection[] = $item;
        return $this;
    }

    protected function isSequentialArray(array $a): bool
    {
        return count(array_filter(array_keys($a), 'is_string')) === 0;
    }

    protected function assertItemIsValid($item): void
    {
        if (is_scalar($item)) {
            return;
        }
        if (!is_array($item)) {
            throw new InvalidArgumentException('A debug item must be a scalar or an array.');
        }
        if ($this->isSequentialArray($item)) {
            return;
        }
        if (false === isset($item['label']) || false === is_string($item['label'])) {
            throw new InvalidArgumentException('A debug sub-item must contain a "label" string element.');
        }
        if (false === isset($item['value'])) {
            throw new InvalidArgumentException('A debug sub-item must contain a "value" element.');
        }
        $this->assertItemIsValid($item['value']);
    }

}
