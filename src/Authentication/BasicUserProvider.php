<?php

/*
 * Copyright 2020, 2023 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\Component\Authentication;

use InvalidArgumentException;

class BasicUserProvider implements UserProviderInterface, UserWithUsernameProviderInterface
{
    private $users = [];
    private $userGroups = [];

    public function __construct(array $users, array $user_groups)
    {
        foreach ($users as $user_identifier => $user) {
            if (!is_int($user_identifier)) {
                throw new InvalidArgumentException('A user definition key must be an integer representing the user\'s identifier.');
            }
            if (!is_array($user)) {
                throw new InvalidArgumentException('A user definition must be an array.');
            }
            if (!isset($user['groups']) || !is_array($user['groups'])) {
                throw new InvalidArgumentException('A user definition must have a "groups" entry. This entry must be an array of strings.');
            }
            if (!isset($user['username']) || !is_string($user['username'])) {
                throw new InvalidArgumentException('A user definition must have a "username" entry.');
            }
            if (!isset($user['password']) || !is_string($user['password'])) {
                throw new InvalidArgumentException('A user definition must have a "password" entry containing the encrypted password.');
            }
            $this->users[$user_identifier] = new BasicUser(
                $user_identifier,
                array_map('strval', $user['groups']),
                (isset($user['omnipotent']) && true === $user['omnipotent']),
                $user['username'],
                $user['password']
            );
        }
        foreach ($user_groups as $user_group_identifier => $user_group_name) {
            if (!is_int($user_group_identifier)) {
                throw new InvalidArgumentException('A user group definition key must be an integer representing the user group\'s identifier.');
            }
            $this->userGroups[$user_group_identifier] = $user_group_name;
        }
    }

    public function loadUserByIdentifier(int $identifier): ?UserInterface
    {
        if (false === isset($this->users[$identifier])) {
            return null;
        }
        return $this->users[$identifier];
    }

    public function getSupportedUserClass(): string
    {
        return 'Vitya\Component\Authentication\BasicUser';
    }

    public function getUserGroups(): array
    {
        return $this->userGroups;
    }

    public function loadUserByUsername(string $username): ?UserInterface
    {
        foreach ($this->users as $user) {
            if ($user->getUsername() === $username) {
                return $user;
            }
        }
        return null;
    }

}
