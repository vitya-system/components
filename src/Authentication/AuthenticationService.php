<?php

/*
 * Copyright 2020, 2023 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\Component\Authentication;

use Psr\Http\Message\ServerRequestInterface;
use RuntimeException;
use Vitya\Component\Session\SessionService;

class AuthenticationService
{
    private $sessionService = null;
    private $realms = [];

    public function __construct(SessionService $session_service)
    {
        $this->sessionService = $session_service;
    }

    public function createRealm(
        string $realm_name,
        UserProviderInterface $user_provider,
        AuthenticatorInterface $authenticator = null
    ): self {
        if (isset($this->realms[$realm_name])) {
            throw new RuntimeException('The "'. $realm_name . '" authentication realm is already defined.');
        }
        $this->realms[$realm_name] = [
            'initialized' => false,
            'user' => null,
            'user_provider' => $user_provider,
            'authenticator' => $authenticator,
        ];
        return $this;
    }

    public function initializeRealm(string $realm_name): self
    {
        if (false === isset($this->realms[$realm_name])) {
            throw new RuntimeException('The "'. $realm_name . '" authentication realm is not defined.');
        }
        if (false === $this->realms[$realm_name]['initialized']) {
            $user_identifier = $this->sessionService->getSession()->get('authentication__' . $realm_name);
            if (is_int($user_identifier)) {
                $user = $this->realms[$realm_name]['user_provider']->loadUserByIdentifier($user_identifier);
                $this->realms[$realm_name]['user'] = $user;
            }
            $this->realms[$realm_name]['initialized'] = true;
        }
        return $this;
    }

    public function getUser(string $realm_name): ?UserInterface
    {
        if (false === isset($this->realms[$realm_name])) {
            throw new RuntimeException('The "'. $realm_name . '" authentication realm is not defined.');
        }
        if (false === $this->realms[$realm_name]['initialized']) {
            $this->initializeRealm($realm_name);
        }
        return $this->realms[$realm_name]['user'];
    }

    public function setUser(string $realm_name, UserInterface $user = null): self
    {
        if (false === isset($this->realms[$realm_name])) {
            throw new RuntimeException('The "'. $realm_name . '" authentication realm is not defined.');
        }
        if ($user !== null && get_class($user) !== $this->realms[$realm_name]['user_provider']->getSupportedUserClass()) {
            throw new RuntimeException('Thus user is not suitable for the "'. $realm_name . '" authentication realm.');
        }
        $this->realms[$realm_name]['user'] = $user;
        $this->realms[$realm_name]['initialized'] = true;
        $this->sessionService->getSession()->set('authentication__' . $realm_name, $user->getUserIdentifier());
        return $this;
    }

    public function getAuthenticator(string $realm_name): ?AuthenticatorInterface
    {
        if (false === isset($this->realms[$realm_name])) {
            throw new RuntimeException('The "'. $realm_name . '" authentication realm is not defined.');
        }
        return $this->realms[$realm_name]['authenticator'];
    }

    public function getUserProvider(string $realm_name): UserProviderInterface
    {
        if (false === isset($this->realms[$realm_name])) {
            throw new RuntimeException('The "'. $realm_name . '" authentication realm is not defined.');
        }
        return $this->realms[$realm_name]['user_provider'];
    }

    public function authenticate(string $realm_name, ServerRequestInterface $server_request = null): self
    {
        $user = $this->getUser($realm_name);
        if ($user !== null) {
            return $this;
        }
        $authenticator = $this->getAuthenticator($realm_name);
        if ($authenticator !== null) {
            $user = $authenticator->tryAuthentication($server_request);
            if ($user !== null && get_class($user) === $this->getUserProvider($realm_name)->getSupportedUserClass()) {
                $this->setUser($realm_name, $user);
            }
        }
        return $this;
    }

    public function logout(): self
    {
        $this->sessionService->end();
        $this->sessionService->destroy();
        foreach ($this->realms as $realm_name => $realm) {
            $this->realms[$realm_name]['user'] = null;
        }
        return $this;
    }

}
