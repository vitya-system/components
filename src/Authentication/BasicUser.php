<?php

/*
 * Copyright 2020, 2023 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\Component\Authentication;

class BasicUser implements UserInterface, UserWithUsernameAndPasswordInterface
{
    private $groups = [];
    private $hashedPassword = '';
    private $identifier = 0;
    private $omnipotent = false;
    private $username = '';

    public function __construct(int $identifier, array $groups, bool $omnipotent, string $username, string $hashed_password)
    {
        $this->groups = array_map('intval', $groups);
        $this->hashedPassword = $hashed_password;
        $this->identifier = $identifier;
        $this->omnipotent = $omnipotent;
        $this->username = $username;
    }

    public function getUsername(): string
    {
        return $this->username;
    }

    public function checkPassword(string $clear_password): bool
    {
        return password_verify($clear_password, $this->hashedPassword);
    }

    public function hashPassword(string $clear_password): string
    {
        return password_hash($clear_password, PASSWORD_DEFAULT);
    }

    public function getUserIdentifier(): int
    {
        return $this->identifier;
    }

    public function getGroups(): array
    {
        return $this->groups;
    }

    public function isOmnipotent(): bool
    {
        return $this->omnipotent;
    }

}
