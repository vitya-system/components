<?php

/*
 * Copyright 2020 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\Component\Authentication;

use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Http\Message\ServerRequestInterface;
use Vitya\Component\Authentication\Exception\AuthenticationProcedureException;
use Vitya\Component\Authentication\UserWithUsernameProviderInterface;

class BasicHttpAuthenticator implements AuthenticatorInterface
{
    private $responseFactory;
    private $userProvider;

    public function __construct(UserWithUsernameProviderInterface $user_provider, ResponseFactoryInterface $response_factory)
    {
        $this->responseFactory = $response_factory;
        $this->userProvider = $user_provider;
    }

    public function tryAuthentication(ServerRequestInterface $server_request = null): ?UserInterface
    {
        $line = trim($server_request->getHeaderLine('Authorization'));
        $elements = explode(' ', $line);
        if ($elements[0] === 'Basic') {
            $credentials_string = base64_decode($elements[1]);
            if ($credentials_string !== false) {
                $credentials = explode(':', $credentials_string);
                if (count($credentials) == 2) {
                    $user = $this->userProvider->loadUserByUsername($credentials[0]);
                    if ($user instanceof UserWithUsernameAndPasswordInterface) {
                        if ($user->checkPassword($credentials[1])) {
                            return $user;
                        }
                    }
                }
            }
        }
        $response = $this->responseFactory->createResponse(401)
            ->withAddedHeader('WWW-Authenticate', 'Basic realm="Authentication required"');
        throw new AuthenticationProcedureException($response);
    }

}
