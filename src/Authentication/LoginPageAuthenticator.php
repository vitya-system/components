<?php

/*
 * Copyright 2022, 2023 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\Component\Authentication;

use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\StreamFactoryInterface;
use Vitya\Component\Authentication\Exception\AuthenticationProcedureException;
use Vitya\Component\Authentication\AuthenticationService;
use Vitya\Component\Authentication\AuthenticatorInterface;
use Vitya\Component\Authentication\UserInterface;
use Vitya\Component\Route\RouterInterface;
use Vitya\Component\Session\SessionService;

class LoginPageAuthenticator implements AuthenticatorInterface
{
    const OK = 0;
    const ERROR_INVALID_USERNAME_OR_PASSWORD = 1;

    private $responseFactory = null;
    private $router = null;
    private $authn = null;
    private $sessionService = null;
    private $realm = '';

    public function __construct(
        AuthenticationService $authn,
        ResponseFactoryInterface $response_factory,
        RouterInterface $router,
        SessionService $session_service,
        string $realm
    ) {
        $this->authn = $authn;
        $this->responseFactory = $response_factory;
        $this->router = $router;
        $this->sessionService = $session_service;
        $this->realm = $realm;
    }

    public function tryAuthentication(ServerRequestInterface $server_request = null): ?UserInterface
    {
        $origin_url = '/';
        if (null !== $server_request) {
            $origin_url = (string) $server_request->getUri();
        }
        $authenticated_user_identifier = (int) $this->sessionService->getSession()->get('authentication__login_page_' . $this->realm . '_authenticated_user_identifier', 0);
        $this->sessionService->getSession()->set('authentication__login_page_' . $this->realm . '_authenticated_user_identifier', null);
        if (0 < $authenticated_user_identifier) {
            $user = $this->authn->getUserProvider($this->realm)->loadUserByIdentifier($authenticated_user_identifier);
            if (null !== $user) {
                return $user;
            }
        }
        $this->sessionService->getSession()->set('authentication__login_page_' . $this->realm . '_origin_url', $origin_url);
        $login_page_uri = $this->router->createUri('login-page-' . $this->realm);
        $response = $this->responseFactory
            ->createResponse(303)
            ->withAddedHeader('Location', (string) $login_page_uri);
        throw new AuthenticationProcedureException($response);
    }

    public function getAuthenticationService(): AuthenticationService
    {
        return $this->authn;
    }

    public function getRealm(): string
    {
        return $this->realm;
    }
    
    public function getResponseFactory(): ResponseFactoryInterface
    {
        return $this->responseFactory;
    }

    public function getRouter(): RouterInterface
    {
        return $this->router;
    }

    public function getSessionService(): SessionService
    {
        return $this->sessionService;
    }

}
