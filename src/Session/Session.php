<?php

/*
 * Copyright 2020 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\Component\Session;

use DateTime;

class Session
{
    private $data = [];
    private $flashMessageQueues = [];
    private $lastModificationDateTime = null;
    
    public function __construct()
    {
        $this->lastModificationDateTime = new DateTime();
    }

    public function get(string $key, $default_value = null)
    {
        if (!isset($this->data[$key])) {
            return $default_value;
        }
        return $this->data[$key];
    }

    public function set(string $key, $value): self
    {
        $this->data[$key] = $value;
        return $this;
    }

    public function pop(string $key, $default_value = null)
    {
        if (!isset($this->data[$key])) {
            return $default_value;
        }
        $value = $this->data[$key];
        unset($this->data[$key]);
        return $value;
    }

    public function getFlashMessageQueue($name): FlashMessageQueue
    {
        if (!isset($this->flashMessageQueues[$name])) {
            $this->flashMessageQueues[$name] = new FlashMessageQueue();
        }
        return $this->flashMessageQueues[$name];
    }

    public function getLastModificationDateTime(): DateTime
    {
        return $this->lastModificationDateTime;
    }

    public function setLastModificationDateTime(DateTime $dt): Session
    {
        $this->lastModificationDateTime = $dt;
        return $this;
    }

}
