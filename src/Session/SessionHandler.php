<?php

/*
 * Copyright 2020 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\Component\Session;

use DateTime;
use InvalidArgumentException;
use RuntimeException;

class SessionHandler implements SessionHandlerInterface
{
    private $savePath = null;
    private $sessionName = null;
    private $filePointers = [];

    public function __construct(string $save_path, string $session_name)
    {
        $this->savePath = $save_path;
        $this->sessionName = $session_name;
    }

    public function __destruct()
    {
        foreach ($this->filePointers as $session_id => $fp) {
            $stat = fstat($fp);
            $size = $stat['size'];
            flock($this->filePointers[$session_id], LOCK_UN);
            fclose($this->filePointers[$session_id]);
            if ($size === 0) {
                unlink($this->getSessionFilePath($session_id));
            }
            $this->filePointers[$session_id] = null;
        }
    }

    public function createSid(): string
    {
        $random_int_1 = (string) random_int(PHP_INT_MIN, PHP_INT_MAX);
        $random_int_2 = (string) random_int(PHP_INT_MIN, PHP_INT_MAX);
        $microtime = microtime();
        return sha1($random_int_1 . $random_int_2 . $microtime);
    }

    public function destroy(string $session_id): void
    {
        $this->assertValidSessionId($session_id);
        unlink($this->getSessionFilePath($session_id));
    }

    public function gc(int $maxlifetime): void
    {
        if ($this->savePath === null) {
            throw new RuntimeException('The open() method of the session handler must be called before garbage collection.');
        }
        $files = scandir($this->savePath);
        foreach ($files as $file) {
            if (substr($file, 0, strlen($this->sessionName)) !== $this->sessionName) {
                continue;
            }
            $file_path = $this->savePath . '/' . $file;
            if (time() - filemtime($file_path) > $maxlifetime) {
                unlink($file_path);
            }
        }
    }

    public function read(string $session_id): ?Session
    {
        $this->assertValidSessionId($session_id);
        if (!isset($this->filePointers[$session_id])) {
            $this->openAndLockFile($session_id);
        }
        $s = stream_get_contents($this->filePointers[$session_id]);
        if ($s === '') {
            return null;
        }
        $session = unserialize($s);
        if (!$session instanceof Session) {
            return null;
        }
        return $session;
    }

    public function write(string $session_id, Session $session): void
    {
        $this->assertValidSessionId($session_id);
        if (!isset($this->filePointers[$session_id])) {
            $this->openAndLockFile($session_id);
        }
        $session->setLastModificationDateTime(new DateTime());
        $s = serialize($session);
        ftruncate($this->filePointers[$session_id], 0);
        fseek($this->filePointers[$session_id], 0);
        if (fwrite($this->filePointers[$session_id], $s) === false) {
            throw new RuntimeException('Could not write the session file (' . $this->getSessionFilePath($session_id) . ').');
        }
    }

    public function openAndLockFile($session_id): void
    {
        $this->assertValidSessionId($session_id);
        $fopen_result = fopen($this->getSessionFilePath($session_id), 'c+');
        if ($fopen_result === false) {
            throw new RuntimeException('Could not open or create the session file (' . $this->getSessionFilePath($session_id) . ').');
        }
        $this->filePointers[$session_id] = $fopen_result;
        if (!flock($fopen_result, LOCK_EX)) {
            throw new RuntimeException('Could not lock the session file (' . $this->getSessionFilePath($session_id) . ').');
        }
    }

    public function getSessionFilePath($session_id)
    {
        return $this->savePath . '/' . $this->sessionName . '_' . $session_id . '.session';
    }

    public function assertValidSessionId($session_id): void
    {
        if (!preg_match('#[a-zA-Z0-9]{1,60}#', $session_id)) {
            throw new InvalidArgumentException('Invalid session id (' . $session_id . ').');
        }
    }

}
