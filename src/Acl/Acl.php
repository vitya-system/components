<?php

/*
 * Copyright 2020, 2023 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\Component\Acl;

use Exception;
use Vitya\Component\Authentication\UserInterface;

class Acl implements AclInterface
{
    private $permissions = [];

    public function getPermissions(): array
    {
        return $this->permissions;
    }

    public function resetPermissions(): AclInterface
    {
        $this->permissions = [];
        return $this;
    }

    public function addPermission(string $type, int $identifier, string $permission): AclInterface
    {
        if (self::TYPE_ALL === $type) {
            $identifier = 0;
        }
        $this->permissions[] = [
            'type' => $type,
            'identifier' => $identifier,
            'permission' => $permission,
        ];
        return $this;
    }

    public function isGranted(string $permission, UserInterface $user = null): bool
    {
        if (null !== $user && $user->isOmnipotent()) {
            return true;
        }
        $username = ($user === null ? null : $user->getUserIdentifier());
        $groups = ($user === null ? [] : $user->getGroups());
        foreach ($this->permissions as $p) {
            if ($p['permission'] !== $permission) {
                continue;
            }
            switch ($p['type']) {
                case self::TYPE_ALL:
                    return true;
                case self::TYPE_GROUP:
                    if (in_array($p['identifier'], $groups)) {
                        return true;
                    }
                    break;
                case self::TYPE_USER:
                    if ($p['identifier'] === $username) {
                        return true;
                    }
                    break;
            }
        }
        return false;
    }

}
