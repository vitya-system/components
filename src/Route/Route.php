<?php

/*
 * Copyright 2020, 2021 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\Component\Route;

use InvalidArgumentException;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\UriInterface;
use Psr\Http\Message\UriFactoryInterface;
use RuntimeException;

class Route
{
    private $name = '';
    private $path = '';
    private $callable = '';
    private $methods = ['GET'];
    private $parameterNames = [];
    private $parameterRegexes = [];
    private $regex = '';
    private $additionalParameters = [];
    private $uriFactory = null;
    private $compiled = false;

    public function __construct(UriFactoryInterface $uri_factory)
    {
        $this->uriFactory = $uri_factory;
    }

    public function createUri(array $parameters = null, array $query_parameters = null): UriInterface
    {
        if ($this->compiled === false) {
            throw new RuntimeException('Route is not compiled.');
        }
        $url = '';
        $matches = preg_split('#\{([a-zA-Z0-9.\-_]+)\}#', $this->path, -1, PREG_SPLIT_DELIM_CAPTURE | PREG_SPLIT_OFFSET_CAPTURE);
        foreach ($matches as $k => $match) {
            if ($k % 2 === 0) {
                $url .= $match[0];
            } else {
                $parameter_name = $match[0];
                if (!isset($parameters[$parameter_name])) {
                    throw new InvalidArgumentException(
                        'Cannot create a url for route ' . $this->name . ' without parameter ' . $parameter_name . '.'
                    );
                }
                $parameter_regex = $this->getParameterRegex($parameter_name);
                $parameter = (string) $parameters[$parameter_name];
                if (!preg_match('#^' . $parameter_regex . '$#', $parameter)) {
                    throw new InvalidArgumentException(
                        'Parameter ' . $parameter_name . ' (' . $parameter . ') does not match its regex (' . $parameter_regex . ').'
                    );
                }
                $url .= $parameter;
            }
        }
        $uri = $this->uriFactory->createUri($url);
        if ($query_parameters !== null) {
            $uri = $uri->withQuery(http_build_query($query_parameters));
        }
        return $uri;
    }

    public function getName(): string
    {
        return $this->name;
    }

    private function setName(string $name): Route
    {
        if ($this->compiled) {
            throw new RuntimeException('Route name cannot be modified after compilation.');
        }
        if ($name === '') {
            throw new InvalidArgumentException('Route name cannot be empty.');
        }
        $this->name = $name;
        return $this;
    }

    public function getPath(): string
    {
        return $this->path;
    }

    private function setPath(string $path): Route
    {
        if ($this->compiled) {
            throw new RuntimeException('Route path cannot be modified after compilation.');
        }
        $this->path = $path;
        return $this;
    }

    public function getMethods(): array
    {
        return $this->methods;
    }

    public function setMethods(array $methods): Route
    {
        if ($this->compiled) {
            throw new RuntimeException('Route methods cannot be modified after compilation.');
        }
        $possible_methods = ['GET', 'POST', 'PUT', 'DELETE', 'PATCH'];
        $this->methods = array_intersect($methods, $possible_methods);
        if (empty($this->methods)) {
            throw new InvalidArgumentException('No valid method was found in the route definition.');
        }
        return $this;
    }

    public function getCallable(): string
    {
        return $this->callable;
    }

    public function setCallable(string $callable): Route
    {
        if ($this->compiled) {
            throw new RuntimeException('Route callable cannot be modified after compilation.');
        }
        $this->callable = $callable;
        return $this;
    }

    public function getParameterNames(): array
    {
        return $this->parameterNames;
    }

    public function getParameterRegexes(): array
    {
        return $this->parameterRegexes;
    }

    public function getParameterRegex(string $parameter_name): string
    {
        if (isset($this->parameterRegexes[$parameter_name])) {
            return $this->parameterRegexes[$parameter_name];
        }
        return '[^/]+';
    }

    public function setParameterRegex(string $parameter_name, string $regex): Route
    {
        if ($this->compiled) {
            throw new RuntimeException('Route parameter regex cannot be modified after compilation.');
        }
        $this->parameterRegexes[$parameter_name] = $regex;
        return $this;
    }

    public function getRegex(): string
    {
        if (false === $this->compiled) {
            throw new RuntimeException('Cannot get a regex: this route ("' . $this->name . '") is not compiled yet.');
        }
        return $this->regex;
    }

    public function getAdditionalParameters(): array
    {
        return $this->additionalParameters;
    }

    public function setAdditionalParameters(array $additional_parameters): Route
    {
        if ($this->compiled) {
            throw new RuntimeException('Route additional parameters cannot be modified after compilation.');
        }
        $this->additionalParameters = $additional_parameters;
        return $this;
    }

    public function getUriFactory()
    {
        return $this->uriFactory;
    }

    public function isCompiled(): bool
    {
        return $this->compiled;
    }

    public function compile()
    {
        if ($this->compiled) {
            throw new RuntimeException('This route ("' . $this->name . '") is already compiled.');
        }
        if ($this->name === '') {
            throw new InvalidArgumentException('Route name cannot be empty.');
        }
        if ($this->path === '') {
            throw new InvalidArgumentException('Route path cannot be empty.');
        }
        $matches = preg_split('#\{([a-zA-Z0-9.\-_]+)\}#', $this->path, -1, PREG_SPLIT_DELIM_CAPTURE | PREG_SPLIT_OFFSET_CAPTURE);
        $parameter_names = [];
        $regex = '';
        foreach ($matches as $k => $match) {
            if ($k % 2 === 0) {
                // Even indices: static strings.
                // Check whether this fixed chunk of string is legal.
                if (preg_match('#[^a-zA-Z0-9.\-_/]#', $match[0])) {
                    throw new InvalidArgumentException('Invalid route path (' . $this->path . ').');
                }
                // Add it to the regex.
                $regex .= preg_quote($match[0], '#');
            } else {
                // Odd indices: parameter placeholders.
                $parameter_names[] = $match[0];
                // Add it to the regex.
                $parameter_regex = $this->getParameterRegex($match[0]);
                $regex .= '(' . $parameter_regex . ')';
            }
        }
        $regex = '#^' . $regex . '$#';
        $this->parameterNames = $parameter_names;
        $this->regex = $regex;
        $this->compiled = true;
        return $this;
    }

    public function setFromArray(string $name, array $definition): self
    {
        $this->setName($name);
        if (isset($definition['path'])) {
            $this->setPath($definition['path']);
        }
        if (isset($definition['callable'])) {
            $this->setCallable($definition['callable']);
        }
        if (isset($definition['methods'])) {
            $this->setMethods($definition['methods']);
        }
        if (isset($definition['regexes']) && is_array($definition['regexes'])) {
            foreach ($definition['regexes'] as $param_name => $param_regex) {
                $this->setParameterRegex($param_name, $param_regex);
            }
        }
        if (isset($definition['additional_parameters'])) {
            $this->setAdditionalParameters($definition['additional_parameters']);
        }
        return $this;
    }

}
