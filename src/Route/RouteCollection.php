<?php

/*
 * Copyright 2021 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\Component\Route;

use InvalidArgumentException;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\UriFactoryInterface;
use Psr\Http\Message\UriInterface;
use Vitya\Component\Route\Exception\NoRouteFoundException;

class RouteCollection
{
    private $baseUrlPath = '';
    private $routes = [];
    private $uriFactory = null;

    public function __construct(string $base_url_path, UriFactoryInterface $uri_factory)
    {
        $this->baseUrlPath = $base_url_path;
        $this->uriFactory = $uri_factory;
    }

    public function getBaseUrlPath(): string
    {
        return $this->baseUrlPath;
    }

    public function getRoutes(): array
    {
        return $this->routes;
    }

    public function getRoute(string $route_name): Route
    {
        if (!isset($this->routes[$route_name])) {
            throw new InvalidArgumentException('Could not find a route named ' . $route_name . '.');
        }
        return $this->routes[$route_name];
    }

    public function setRoute(Route $route): RouteCollection
    {
        $this->routes[$route->getName()] = $route;
        return $this;
    }

    public function getUriFactory(): UriFactoryInterface
    {
        return $this->uriFactory;
    }

    public function findProcessingInstruction(RequestInterface $request): ProcessingInstruction
    {
        $processing_instruction = null;
        $contextualized_path = preg_replace('#^' . preg_quote($this->baseUrlPath) . '#', '', $request->getUri()->getPath());
        $contextualized_request = $request->withUri($request->getUri()->withPath($contextualized_path));
        foreach ($this->routes as $route) {
            $match_result = $this->matchRequest($route, $contextualized_request);
            if ($match_result !== null) {
                $processing_instruction = $match_result;
                break;
            }
        }
        if ($processing_instruction === null) {
            throw new NoRouteFoundException(''
                . 'No route found '
                . '(' . $request->getMethod() . ' ' . $request->getUri() . ')'
            );
        }
        return $processing_instruction;
    }

    public function createUri(string $route_name, array $parameters = null, array $query_parameters = null): UriInterface
    {
        if (!isset($this->routes[$route_name])) {
            throw new InvalidArgumentException('Could not find a route named ' . $route_name . '.');
        }
        $uri = $this->routes[$route_name]->createUri($parameters, $query_parameters);
        $uri = $uri->withPath($this->baseUrlPath . $uri->getPath());
        return $uri;
    }

    public function matchRequest(Route $route, RequestInterface $request): ?ProcessingInstruction
    {
        if ($route->isCompiled() === false) {
            throw new RuntimeException('Route is not compiled.');
        }
        if ($route->getRegex() === '') {
            throw new InvalidArgumentException('Empty regex for this route (' . $route->getName() . ').');
        }
        $path = $request->getUri()->getPath();
        $processing_instruction = null;
        if (preg_match($route->getRegex(), $path, $matches) && in_array($request->getMethod(), $route->getMethods())) {
            $parameters = array();
            foreach ($route->getParameterNames() as $k => $parameter_name) {
                if (isset($matches[$k + 1])) {
                    $parameters[$parameter_name] = rawurldecode($matches[$k + 1]);
                }
            }
            $parameters = array_merge($route->getAdditionalParameters(), $parameters);
            $processing_instruction = new ProcessingInstruction($route->getCallable(), $parameters);
        }
        return $processing_instruction;
    }

}
