<?php

/*
 * Copyright 2020, 2021 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\Component\Route;

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\UriInterface;

class Router implements RouterInterface
{
    private $routeCollection = null;

    public function __construct(RouteCollection $route_collection)
    {
        $this->routeCollection = $route_collection;
    }

    public function findProcessingInstruction(RequestInterface $request): ProcessingInstruction
    {
        return $this->routeCollection->findProcessingInstruction($request);
    }

    public function createUri(string $route_name, array $parameters = null, array $query_parameters = null): UriInterface
    {
        return $this->routeCollection->createUri($route_name, $parameters, $query_parameters);
    }

    public function getRouteCollection(): RouteCollection
    {
        return $this->routeCollection;
    }

}
