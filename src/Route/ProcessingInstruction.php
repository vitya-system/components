<?php

/*
 * Copyright 2020 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\Component\Route;

class ProcessingInstruction
{
    private $callable = '';
    private $parameters = [];

    public function __construct(string $callable, array $parameters = [])
    {
        $this->callable = $callable;
        $this->parameters = $parameters;
    }

    public function getCallable(): string
    {
        return $this->callable;
    }

    public function getParameters(): array
    {
        return $this->parameters;
    }

    public function getParameter(string $parameter, $default = null)
    {
        if (array_key_exists($parameter, $this->parameters) === false) {
            return $default;
        }
        return $this->parameters[$parameter];
    }

    public function withParameter(string $parameter, $value)
    {
        $new_processing_instruction = clone $this;
        $new_processing_instruction->parameters[$parameter] = $value;
        return $new_processing_instruction;
    }

    public function withoutParameter(string $parameter)
    {
        if (array_key_exists($parameter, $this->parameters) === false) {
            return $this;
        }
        $new_processing_instruction = clone $this;
        unset($new_processing_instruction->parameters[$parameter]);
        return $new_processing_instruction;
    }

}
