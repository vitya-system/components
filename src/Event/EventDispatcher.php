<?php

/*
 * Copyright 2020 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\Component\Event;

use Psr\EventDispatcher\EventDispatcherInterface;
use Psr\EventDispatcher\ListenerProviderInterface;
use Psr\EventDispatcher\StoppableEventInterface;

class EventDispatcher implements EventDispatcherInterface
{
    private $listenerProvidersAreSorted = false;
    private $listenerProviders = [];

    public function dispatch(object $event)
    {
        $listeners = [];
        if (!$this->listenerProvidersAreSorted) {
            arsort($this->listenerProviders);
            $this->listenerProvidersAreSorted = true;
        }
        foreach ($this->listenerProviders as $priority => $listener_provider_group) {
            foreach ($listener_provider_group as $listener_provider) {
                $listeners = array_merge($listeners, $listener_provider->getListenersForEvent($event));
            }
        }
        foreach ($listeners as $listener) {
            call_user_func($listener, $event);
            if ($event instanceof StoppableEventInterface) {
                if ($event->isPropagationStopped()) {
                    break;
                }
            }
        }
    }

    public function getListenerProviders(): array
    {
        return $this->listenerProviders;
    }

    public function addListenerProvider(ListenerProviderInterface $listener_provider, int $priority): self
    {
        if (!isset($this->listenerProviders[$priority])) {
            $this->listenerProviders[$priority] = [];
            $this->listenerProvidersAreSorted = false;
        }
        $this->listenerProviders[$priority][] = $listener_provider;
        return $this;
    }

}
