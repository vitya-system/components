<?php

/*
 * Copyright 2025 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\Component\TemporaryStorage;

use Exception;
use Generator;
use Psr\Http\Message\UploadedFileInterface;

class BasicTemporaryStorage implements TemporaryStorageInterface
{
    private $tmpDir = '';
    private $createdFiles = [];

    public function __construct(string $tmp_dir)
    {
        $this->tmpDir = realpath($tmp_dir);
    }

    public function getTmpDir(): string
    {
        return $this->tmpDir;
    }

    public function getCreatedFiles(): array
    {
        return $this->createdFiles;
    }

    public function copyToTmp(string $path): string
    {
        $filename = 't_' . time() . '_' . md5(uniqid() . rand(0, 1000000));
        $result = copy($path, $this->getTmpDir() . '/' . $filename);
        if (false === $result) {
            throw new Exception('Could not copy "' . $path . '" to temporary storage.');
        }
        $this->createdFiles[] = $filename;
        return $filename;
    }

    public function copyFromTmp(string $filename, string $target_path): void
    {
        $result = copy($this->getTmpDir() . '/' . $filename, $target_path);
        if (false === $result) {
            throw new Exception('Could not copy tmporary file "' . $filename . '" to "' . $target_path . '".');
        }
    }

    public function moveUploadedFile(UploadedFileInterface $uploaded_file): string
    {
        $filename = 't_' . time() . '_' . md5(uniqid() . rand(0, 1000000));
        $uploaded_file->moveTo($this->getTmpDir() . '/' . $filename);
        $this->createdFiles[] = $filename;
        return $filename;
    }

    public function getFilePath(string $filename): string
    {
        return $this->getTmpDir() . '/' . $filename;
    }

    public function delete(string $filename): void
    {
        unlink($this->getTmpDir() . '/' . $filename);
    }

    public function purgeOldFiles(): Generator
    {
        $filenames = scandir($this->getTmpDir());
        foreach ($filenames as $filename) {
            if ('.' === substr($filename, 0, 1)) {
                continue;
            }
            if (time() - filectime($this->getTmpDir() . '/' . $filename) < 3600) {
                continue;
            }
            unlink($this->getTmpDir() . '/' . $filename);
            yield $filename;
        }
    }

    public function __destruct()
    {
        foreach ($this->createdFiles as $filename) {
            unlink($this->getTmpDir() . '/' . $filename);
        }
    }

}
