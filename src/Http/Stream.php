<?php

/*
 * Copyright 2020 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\Component\Http;

use InvalidArgumentException;
use Psr\Http\Message\StreamInterface;
use RuntimeException;
use Throwable;

class Stream implements StreamInterface
{
    private const READABLE_MODES_REGEXP = '/r|a\+|ab\+|w\+|wb\+|x\+|xb\+|c\+|cb\+/';
    private const WRITABLE_MODES_REGEXP = '/a|w|r\+|rb\+|rw|x|c/';

    private $stream;
    private $seekable;
    private $readable;
    private $writable;

    public function __construct($stream)
    {
        if (!is_resource($stream)) {
            throw new InvalidArgumentException('Stream must be a resource.');
        }
        $this->stream = $stream;
        $metadata = stream_get_meta_data($this->stream);
        $this->seekable = $metadata['seekable'];
        $this->readable = (bool) preg_match(self::READABLE_MODES_REGEXP, $metadata['mode']);
        $this->writable = (bool) preg_match(self::WRITABLE_MODES_REGEXP, $metadata['mode']);
    }

    public function __toString()
    {
        try {
            if ($this->isSeekable()) {
                $this->seek(0);
            }
            return $this->getContents();
        } catch (Throwable $e) {
            return '';
        }
    }

    public function close()
    {
        if (isset($this->stream)) {
            if (is_resource($this->stream)) {
                fclose($this->stream);
            }
            $this->detach();
        }
    }

    public function detach()
    {
        if (!isset($this->stream)) {
            return null;
        }
        $result = $this->stream;
        $this->stream = null;
        $this->readable = false;
        $this->writable = false;
        $this->seekable = false;
        return $result;
    }

    public function getSize()
    {
        $stats = fstat($this->stream);
        if (isset($stats['size'])) {
            return $this->size;
        }
        return null;
    }

    public function tell()
    {
        if (!isset($this->stream)) {
            throw new RuntimeException('Stream is detached.');
        }
        $result = ftell($this->stream);
        if ($result === false) {
            throw new RuntimeException('Could not determine the current position.');
        }
        return $result;
    }

    public function eof()
    {
        if (!isset($this->stream)) {
            throw new RuntimeException('Stream is detached.');
        }
        return feof($this->stream);
    }

    public function isSeekable()
    {
        return $this->seekable;
    }

    public function seek($offset, $whence = SEEK_SET)
    {
        $whence = (int) $whence;
        if (!isset($this->stream)) {
            throw new RuntimeException('Stream is detached.');
        }
        if (!$this->seekable) {
            throw new RuntimeException('Stream is not seekable.');
        }
        if (fseek($this->stream, $offset, $whence) === -1) {
            throw new RuntimeException('Could not go to position ' . $whence . '.');
        }
    }

    public function rewind()
    {
        $this->seek(0);
    }

    public function isWritable()
    {
        return $this->writable;
    }

    public function write($string)
    {
        if (!isset($this->stream)) {
            throw new RuntimeException('Stream is detached.');
        }
        if (!$this->writable) {
            throw new RuntimeException('Stream is not writable.');
        }
        $result = fwrite($this->stream, $string);
        if ($result === false) {
            throw new RuntimeException('Could not write to stream.');
        }
        return $result;
    }

    public function isReadable()
    {
        return $this->readable;
    }

    public function read($length)
    {
        if (!isset($this->stream)) {
            throw new RuntimeException('Stream is detached.');
        }
        if (!$this->readable) {
            throw new RuntimeException('Stream is not readable.');
        }
        if ($length < 0) {
            throw new RuntimeException('Invalid length (' . $length . ').');
        }
        if ($length === 0) {
            return '';
        }
        $string = fread($this->stream, $length);
        if ($string === false) {
            throw new RuntimeException('Could not read from stream.');
        }
        return $string;
    }

    public function getContents()
    {
        if (!isset($this->stream)) {
            throw new RuntimeException('Stream is detached.');
        }
        $contents = stream_get_contents($this->stream);
        if ($contents === false) {
            throw new RuntimeException('Could not read stream contents.');
        }
        return $contents;
    }

    public function getMetadata($key = null)
    {
        if (!isset($this->stream)) {
            return ($key === null) ? [] : null;
        }
        $meta = stream_get_meta_data($this->stream);
        return isset($meta[$key]) ? $meta[$key] : null;
    }

}
