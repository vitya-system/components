<?php

/*
 * Copyright 2020 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\Component\Http;

use InvalidArgumentException;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\StreamInterface;

class Response implements ResponseInterface
{
    use MessageTrait;

    public const DEFAULT_REASON_PHRASES = [
        100 => 'Continue',
        101 => 'Switching protocols',
        102 => 'Processing',
        200 => 'OK',
        201 => 'Created',
        202 => 'Accepted',
        203 => 'Non-authoritative information',
        204 => 'No content',
        205 => 'Reset content',
        206 => 'Partial content',
        207 => 'Multi-status',
        208 => 'Already reported',
        300 => 'Multiple choices',
        301 => 'Moved permanently',
        302 => 'Found',
        303 => 'See other',
        304 => 'Not modified',
        305 => 'Use proxy',
        306 => 'Switch proxy',
        307 => 'Temporary redirect',
        400 => 'Bad request',
        401 => 'Unauthorized',
        402 => 'Payment required',
        403 => 'Forbidden',
        404 => 'Not found',
        405 => 'Method not allowed',
        406 => 'Not acceptable',
        407 => 'Proxy authentication required',
        408 => 'Request time-out',
        409 => 'Conflict',
        410 => 'Gone',
        411 => 'Length required',
        412 => 'Precondition failed',
        413 => 'Request entity too large',
        414 => 'Request-URI too large',
        415 => 'Unsupported media type',
        416 => 'Requested range not satisfiable',
        417 => 'Expectation failed',
        418 => 'I\'m a teapot',
        422 => 'Unprocessable entity',
        423 => 'Locked',
        424 => 'Failed dependency',
        425 => 'Unordered collection',
        426 => 'Upgrade required',
        428 => 'Precondition required',
        429 => 'Too many requests',
        431 => 'Request header fields too large',
        451 => 'Unavailable for legal reasons',
        500 => 'Internal server error',
        501 => 'Not implemented',
        502 => 'Bad gateway',
        503 => 'Service unavailable',
        504 => 'Gateway time-out',
        505 => 'HTTP version not supported',
        506 => 'Variant also negotiates',
        507 => 'Insufficient storage',
        508 => 'Loop detected',
        510 => 'Not extended',
        511 => 'Network authentication required',
    ];

    private $reasonPhrase = '';
    private $statusCode = 200;

    public function __construct(int $status_code = 200, string $reason = '')
    {
        $this->statusCode = $status_code;
        if ($reason === '' && isset(self::DEFAULT_REASON_PHRASES[$this->statusCode])) {
            $this->reasonPhrase = self::DEFAULT_REASON_PHRASES[$this->statusCode];
        } else {
            $this->reasonPhrase = $reason;
        }
    }

    public function getStatusCode()
    {
        return $this->statusCode;
    }

    public function withStatus($code, $reasonPhrase = '')
    {
        if (filter_var($code, FILTER_VALIDATE_INT) === false) {
            throw new InvalidArgumentException('Status code must be an integer value.');
        }
        $code = (int) $code;
        $new_response = clone $this;
        $new_response->statusCode = $code;
        if ($reasonPhrase == '' && isset(self::DEFAULT_REASON_PHRASES[$new_response->statusCode])) {
            $reasonPhrase = self::DEFAULT_REASON_PHRASES[$new_response->statusCode];
        }
        $new_response->reasonPhrase = $reasonPhrase;
        return $new_response;
    }

    public function getReasonPhrase()
    {
        return $this->reasonPhrase;
    }

}
