<?php

/*
 * Copyright 2020 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\Component\Http;

use InvalidArgumentException;
use Psr\Http\Message\UriInterface;

class Uri implements UriInterface
{
    private $scheme = '';
    private $userInfo = '';
    private $host = '';
    private $port = null;
    private $path = '';
    private $query = '';
    private $fragment = '';

    public function __construct(string $uri = '')
    {
        if ($uri !== '') {
            $parts = parse_url($uri);
            if ($parts === false) {
                throw new InvalidArgumentException('This uri could not be parsed: ' . $uri . '.');
            }
            $this->scheme = isset($parts['scheme']) ? $parts['scheme'] : '';
            $this->userInfo = isset($parts['user']) ? $parts['user'] : '';
            $this->host = isset($parts['host']) ? $parts['host'] : '';
            $this->port = isset($parts['port']) ? $parts['port'] : null;
            $this->path = isset($parts['path']) ? $parts['path'] : '';
            $this->query = isset($parts['query']) ? $parts['query'] : '';
            $this->fragment = isset($parts['fragment']) ? $parts['fragment'] : '';
            if (isset($parts['pass'])) {
                $this->userInfo .= ':' . $parts['pass'];
            }
        }
    }

    public function getScheme()
    {
        return $this->scheme;
    }

    public function getAuthority()
    {
        $authority = $this->host;
        if ($this->userInfo !== '') {
            $authority = $this->userInfo . '@' . $authority;
        }
        if ($this->port !== null) {
            $authority .= ':' . $this->port;
        }
        return $authority;
    }

    public function getUserInfo()
    {
        return $this->userInfo;
    }

    public function getHost()
    {
        return $this->host;
    }

    public function getPort()
    {
        return $this->port;
    }

    public function getPath()
    {
        return $this->path;
    }

    public function getQuery()
    {
        return $this->query;
    }

    public function getFragment()
    {
        return $this->fragment;
    }

    public function withScheme($scheme)
    {
        $new_uri = clone $this;
        $new_uri->scheme = $scheme;
        return $new_uri;
    }

    public function withUserInfo($user, $password = null)
    {
        $new_uri = clone $this;
        $user_info = $user;
        if ($password !== null) {
            $user_info .= ':' . $password;
        }
        $new_uri->userInfo = $user_info;
        return $new_uri;
    }

    public function withHost($host)
    {
        $new_uri = clone $this;
        $new_uri->host = $host;
        return $new_uri;
    }

    public function withPort($port)
    {
        $new_uri = clone $this;
        $new_uri->port = $port;
        return $new_uri;
    }

    public function withPath($path)
    {
        $new_uri = clone $this;
        $new_uri->path = $path;
        return $new_uri;
    }

    public function withQuery($query)
    {
        $new_uri = clone $this;
        $new_uri->query = $query;
        return $new_uri;
    }

    public function withFragment($fragment)
    {
        $new_uri = clone $this;
        $new_uri->fragment = $fragment;
        return $new_uri;
    }

    public function __toString()
    {
        $s = '';
        $scheme = $this->getScheme();
        $authority = $this->getAuthority();
        $path = $this->getPath();
        $query = $this->getQuery();
        $fragment = $this->getFragment();
        if ($scheme != '') {
            $s .= $scheme . ':';
        }
        if ($authority != '' || $scheme === 'file') {
            $s .= '//' . $authority;
        }
        $s .= $path;
        if ($query != '') {
            $s .= '?' . $query;
        }
        if ($fragment != '') {
            $s .= '#' . $fragment;
        }
        return $s;
    }

}
