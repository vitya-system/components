<?php

/*
 * Copyright 2020 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\Component\Http;

use DateTimeInterface;
use InvalidArgumentException;

class Cookie implements CookieInterface
{
    private $name = '';
    private $value = '';
    private $maxAge = null;
    private $expires = null;
    private $secure = false;
    private $httpOnly = false;
    private $path = '';
    private $domain = null;
    private $sameSite = 'Lax';

    public function __construct(string $name, string $value, int $max_age = null)
    {
        if (trim($name) === '') {
            throw new InvalidArgumentException('Cookie name cannot be empty.');
        }
        $this->name = trim($name);
        $this->value = $value;
        $this->maxAge = $max_age;
    }

    public function __toString(): string
    {
        return $this->createHeaderLine();
    }

    public function createHeaderLine(): string
    {
        $elements = [rawurlencode($this->name) . '=' . rawurlencode($this->value)];
        if ($this->maxAge !== null) {
            $elements[] = 'Max-Age=' . $this->maxAge;
        }
        if ($this->expires !== null) {
            $elements[] = 'Expires=' . $this->expires->format(DateTimeInterface::RFC7231);
        }
        if ($this->secure) {
            $elements[] = 'Secure';
        }
        if ($this->httpOnly) {
            $elements[] = 'HttpOnly';
        }
        if ($this->path !== '') {
            $elements[] = 'Path=' . $this->path;
        }
        if ($this->domain !== null) {
            $elements[] = 'Domain=' . $this->domain;
        }
        if ($this->sameSite !== null) {
            $elements[] = 'SameSite=' . $this->sameSite;
        }
        return implode('; ', $elements);
    }

    public function withName(string $name): CookieInterface
    {
        if (trim($name) === '') {
            throw new InvalidArgumentException('Cookie name cannot be empty.');
        }
        $new_cookie = clone $this;
        $new_cookie->name = $name;
        return $new_cookie;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function withValue(string $value): CookieInterface
    {
        $new_cookie = clone $this;
        $new_cookie->value = $value;
        return $new_cookie;
    }

    public function getValue(): string
    {
        return $this->value;
    }

    public function withMaxAge(int $max_age = null): CookieInterface
    {
        $new_cookie = clone $this;
        $new_cookie->maxAge = $max_age;
        $new_cookie->expires = null;
        return $new_cookie;
    }

    public function getMaxAge(): ?int
    {
        return $this->maxAge;
    }

    public function withExpires(DateTimeInterface $expires = null): CookieInterface
    {
        $new_cookie = clone $this;
        $new_cookie->expires = $expires;
        $new_cookie->maxAge = null;
        return $new_cookie;
    }

    public function getExpires(): ?DateTimeInterface
    {
        return $this->expires;
    }

    public function withSecure(bool $secure): CookieInterface
    {
        $new_cookie = clone $this;
        $new_cookie->secure = $secure;
        return $new_cookie;
    }

    public function getSecure(): bool
    {
        return $this->secure;
    }

    public function withHttpOnly(bool $http_only): CookieInterface
    {
        $new_cookie = clone $this;
        $new_cookie->httpOnly = $http_only;
        return $new_cookie;
    }

    public function getHttpOnly(): bool
    {
        return $this->httpOnly;
    }

    public function withPath(string $path): CookieInterface
    {
        if (!preg_match('#^[a-zA-Z0-9.\-_/]*$#', $path)) {
            throw new InvalidArgumentException('Cookie path contains invalid characters.');
        }
        $new_cookie = clone $this;
        $new_cookie->path = $path;
        return $new_cookie;
    }

    public function getPath(): ?string
    {
        return $this->path;
    }

    public function withDomain(string $domain = null): CookieInterface
    {
        if ($domain !== null && !filter_var($domain, FILTER_VALIDATE_DOMAIN, FILTER_FLAG_HOSTNAME)) {
            throw new InvalidArgumentException('Cookie domain is not valid.');
        }
        $new_cookie = clone $this;
        $new_cookie->domain = $domain;
        return $new_cookie;
    }

    public function getDomain(): ?string
    {
        return $this->domain;
    }

    public function withSameSite(string $same_site = null): CookieInterface
    {
        if ($same_site !== null && !in_array($same_site, ['None', 'Strict', 'Lax'])) {
            throw new InvalidArgumentException('Cookie SameSite policy must be null, or set to \'None\', \'Strict\' or \'Lax\'.');
        }
        $new_cookie = clone $this;
        $new_cookie->sameSite = $same_site;
        return $new_cookie;
    }

    public function getSameSite(): ?string
    {
        return $this->sameSite;
    }

}
