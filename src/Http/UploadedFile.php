<?php

/*
 * Copyright 2020 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\Component\Http;

use InvalidArgumentException;
use Psr\Http\Message\StreamInterface;
use Psr\Http\Message\UploadedFileInterface;
use RuntimeException;

class UploadedFile implements UploadedFileInterface
{
    private $clientFilename = '';
    private $clientMediaType = '';
    private $error = UPLOAD_ERR_NO_FILE;
    private $size = 0;
    private $stream = null;

    public function __construct(
        StreamInterface $stream,
        int $size,
        int $error,
        string $client_filename,
        string $client_media_type
    ) {
        $this->stream = $stream;
        $this->clientFilename = $client_filename;
        $this->clientMediaType = $client_media_type;
        $this->error = $error;
        $this->size = $size;
    }

    public function getStream()
    {
        return $this->stream;
    }

    public function moveTo($targetPath)
    {
        if ($targetPath == '') {
            throw new InvalidArgumentException('Invalid target path.');
        }
        $result = false;
        $dest_resource = fopen($targetPath, 'w');
        if ($dest_resource === false) {
            throw new InvalidArgumentException('Target path could not be opened.');
        }
        $source = $this->getStream();
        $dest = new Stream($dest_resource);
        $buffer_size = 16 * 1024;
        while (!$source->eof()) {
            if (!$dest->write($source->read($buffer_size))) {
                break;
            }
        }
        $source->close();
    }

    public function getSize()
    {
        return $this->size;
    }

    public function getError()
    {
        return $this->error;
    }

    public function getClientFilename()
    {
        return $this->clientFilename;
    }

    public function getClientMediaType()
    {
        return $this->clientMediaType;
    }

}
