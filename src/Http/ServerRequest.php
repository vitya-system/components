<?php

/*
 * Copyright 2020 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\Component\Http;

use InvalidArgumentException;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\StreamInterface;
use Psr\Http\Message\UploadedFileInterface;
use Psr\Http\Message\UriInterface;

class ServerRequest extends Request implements ServerRequestInterface
{
    private $attributes = [];
    private $cookieParams = [];
    private $parsedBody;
    private $queryParams = [];
    private $serverParams;
    private $uploadedFiles = [];

    public function __construct(
        string $method,
        $uri,
        array $server_params = []
    ) {
        $this->serverParams = $server_params;
        parent::__construct($method, $uri);
    }

    public function getServerParams()
    {
        return $this->serverParams;
    }

    public function getUploadedFiles()
    {
        return $this->uploadedFiles;
    }

    public function withUploadedFiles(array $uploadedFiles)
    {
        $new_server_request = clone $this;
        $new_server_request->uploadedFiles = $uploadedFiles;
        return $new_server_request;
    }

    public function getCookieParams()
    {
        return $this->cookieParams;
    }

    public function withCookieParams(array $cookies)
    {
        $new_server_request = clone $this;
        $new_server_request->cookieParams = $cookies;
        return $new_server_request;
    }

    public function getQueryParams()
    {
        return $this->queryParams;
    }

    public function withQueryParams(array $query)
    {
        $new_server_request = clone $this;
        $new_server_request->queryParams = $query;
        return $new_server_request;
    }

    public function getParsedBody()
    {
        return $this->parsedBody;
    }

    public function withParsedBody($data)
    {
        $new_server_request = clone $this;
        $new_server_request->parsedBody = $data;
        return $new_server_request;
    }

    public function getAttributes()
    {
        return $this->attributes;
    }

    public function getAttribute($attribute, $default = null)
    {
        if (array_key_exists($attribute, $this->attributes) === false) {
            return $default;
        }
        return $this->attributes[$attribute];
    }

    public function withAttribute($attribute, $value)
    {
        $new_server_request = clone $this;
        $new_server_request->attributes[$attribute] = $value;
        return $new_server_request;
    }

    public function withoutAttribute($attribute)
    {
        if (array_key_exists($attribute, $this->attributes) === false) {
            return $this;
        }
        $new_server_request = clone $this;
        unset($new_server_request->attributes[$attribute]);
        return $new_server_request;
    }

}
