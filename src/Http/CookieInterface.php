<?php

/*
 * Copyright 2020 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\Component\Http;

use DateTimeInterface;

interface CookieInterface
{
    public function __toString(): string;

    public function createHeaderLine(): string;

    public function withName(string $name): CookieInterface;

    public function getName(): string;

    public function withValue(string $value): CookieInterface;

    public function getValue(): string;

    public function withMaxAge(int $max_age = null): CookieInterface;

    public function getMaxAge(): ?int;

    public function withExpires(DateTimeInterface $expires = null): CookieInterface;

    public function getExpires(): ?DateTimeInterface;

    public function withSecure(bool $secure): CookieInterface;

    public function getSecure(): bool;

    public function withHttpOnly(bool $http_only): CookieInterface;

    public function getHttpOnly(): bool;

    public function withPath(string $path): CookieInterface;

    public function getPath(): ?string;

    public function withDomain(string $domain = null): CookieInterface;

    public function getDomain(): ?string;

    public function withSameSite(string $same_site = null): CookieInterface;

    public function getSameSite(): ?string;

}
