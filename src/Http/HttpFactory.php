<?php

/*
 * Copyright 2020 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\Component\Http;

use InvalidArgumentException;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\StreamInterface;
use Psr\Http\Message\UploadedFileInterface;
use Psr\Http\Message\UriInterface;
use RuntimeException;
use Vitya\Component\Http\CookieInterface;

class HttpFactory implements HttpFactoryInterface
{
    public function createCookie(string $name, string $value, int $max_age = null): CookieInterface
    {
        return new Cookie($name, $value, $max_age);
    }

    public function createRequest(string $method, $uri): RequestInterface
    {
        $uri_object = null;
        if ($uri instanceof UriInterface) {
            $uri_object = $uri;
        }
        if (is_string($uri)) {
            $uri_object = new Uri($uri);
        }
        if ($uri_object === null) {
            throw new InvalidArgumentException('Uri must be a Uri object or a string.');
        }
        return new Request($method, $uri_object);
    }

    public function createResponse(int $code = 200, string $reasonPhrase = ''): ResponseInterface
    {
        return new Response($code, $reasonPhrase);
    }

    public function createServerRequest(string $method, $uri, array $serverParams = []): ServerRequestInterface
    {
        $uri_object = null;
        if ($uri instanceof UriInterface) {
            $uri_object = $uri;
        }
        if (is_string($uri)) {
            $uri_object = new Uri($uri);
        }
        if ($uri_object === null) {
            throw new InvalidArgumentException('Uri must be a Uri object or a string.');
        }
        return new ServerRequest($method, $uri_object, $serverParams);
    }

    public function createStream(string $content = ''): StreamInterface
    {
        $resource = fopen('php://temp', 'r+');
        fwrite($resource, $content);
        fseek($resource, 0);
        return new Stream($resource);
    }

    public function createStreamFromFile(string $file, string $mode = 'r'): StreamInterface
    {
        $resource = fopen($file, $mode);
        if ($resource === false) {
            throw new RuntimeException('Could not open file (' . $file . ').');
        }
        return new Stream($resource);
    }

    public function createStreamFromResource($resource): StreamInterface
    {
        return new Stream($resource);
    }

    public function createUploadedFile(
        StreamInterface $stream,
        int $size = null,
        int $error = \UPLOAD_ERR_OK,
        string $clientFilename = null,
        string $clientMediaType = null
    ): UploadedFileInterface {
        if ($size === null) {
            $size = $stream->getSize();
        }
        return new UploadedFile($stream, $size, $error, $clientFilename, $clientMediaType);
    }

    public function createUri(string $uri = ''): UriInterface
    {
        return new Uri($uri);
    }

    public function getBasePath(): string
    {
        return $this->basePath;
    }

}
