<?php

/*
 * Copyright 2020 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\Component\Controller\Event;

use Throwable;
use Psr\Http\Message\ResponseInterface;
use Psr\EventDispatcher\StoppableEventInterface;

class ControllerExceptionEvent implements StoppableEventInterface
{
    private $response = null;
    private $throwable = null;

    public function __construct(Throwable $throwable)
    {
        $this->throwable = $throwable;
    }

    public function isPropagationStopped(): bool
    {
        return $this->response !== null;
    }

    public function getThrowable(): Throwable
    {
        return $this->throwable;
    }

    public function getResponse(): ?ResponseInterface
    {
        return $this->response;
    }

    public function setResponse(ResponseInterface $response): self
    {
        $this->response = $response;
        return $this;
    }

}
