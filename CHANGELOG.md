# Changelog

## [1.0.0-alpha14] - 2025-03-07
### Chaged
- Transform cms-ui into cms-application.

## [1.0.0-alpha13] - 2025-02-21
### Added
- Add image manipulation functionalities.

## [1.0.0-alpha12] - 2024-09-30
### Added
- Add a logger.

## [1.0.0-alpha11] - 2024-07-16
### Added
- Controller chaining is nox possible, ie. a controller can return a controller.
- File storage service that abstracts the underlying filesystem.
### Changed
- Various changes to user and user groups management.

## [1.0.0-alpha10] - 2022-07-01
### Added
- There is a new login page authenticator.

## [1.0.0-alpha9] - 2022-06-03
### Changed
- A few small changes.
- Bumped version to alpha9 to match the rest of the system.

## [1.0.0-alpha8] - 2021-08-02
### Changed
- Bumped version to alpha8 to match the rest of the system.

## [1.0.0-alpha7] - 2021-07-05
### Added
- LocaleCatalog objects have a hasLocale() method.
### Changed
- There can be omnipotent users (superadmins).
### Removed
- WidgetFactory was removed in favor of using DependencyInjector.

## [1.0.0-alpha6] - 2021-06-07
### Changed
- Reorganize router.

## [1.0.0-alpha5] - 2021-05-03
### Changed
- Added a locale getter to LocaleCatalog.

## [1.0.0-alpha4] - 2021-04-07
### Added
- Image component: it can read an image file, apply some transformations and
  output the result in many formats (JPEG, PNG, WebP...)

## [1.0.0-alpha3] - 2021-03-01
### Added
- L10n component.
### Changed
- Container parameters can now be injected using the
  DependencyInjector::bindParameter() method.
- Base URL path is now defined at the router level rather than the route level.
- Asset pools are now objects instead of arrays.

## [1.0.0-alpha2] - 2021-02-01
### Added
- You can now create a widget using a WidgetFactory. The factory will inject
  dependencies into the constructor.
### Changed
- The dependency injector now also injects the container parameters according to
  their name. It's a temporary change before switching to a more robust
  approach.
- PHP version requirement lowered to 7.3.

## [1.0.0-alpha1] - 2021-01-04
### Added
- First release.
